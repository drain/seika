﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Helpers/Notification")]
public class NotificationHelper : MonoBehaviour 
{
	[SerializeField]
	protected Vector3 m_notificationPosition;
	
	[SerializeField]
	protected Transform m_notificationTransform;
	
	[SerializeField]
	protected float m_notificationDuration;
	
	public void Notificate()
	{
		Vector3 position = m_notificationPosition;
		
		if (m_notificationTransform)
		{
			position = m_notificationTransform.position;
		}
		
		CameraFollow.MoveTo(position, m_notificationDuration);
	}
}