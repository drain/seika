﻿using UnityEngine;
using System.Collections;

public static class GameResources 
{
	public delegate void PauseDelegate(bool isPaused);
	
	public static event PauseDelegate OnPause;
	
	public static Character CurrentPlayer
	{
		get;
		set;
	}

	private static bool m_isPaused;

	public static bool IsPaused
	{
		get
		{
			return m_isPaused;
		}
		
		set
		{
			m_isPaused = value;

			if (OnPause != null)
			{
				OnPause(m_isPaused);
			}
		}
	}

	public static bool StopMovingEntities 
	{
		get;
		set;
	}

	public static float Currency 
	{
		get;
		set;
	}
}