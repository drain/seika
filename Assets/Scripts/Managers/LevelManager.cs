﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour
{
	public static int DifficultyLevel 
	{
		get;
		set;
	}

	public static float totalTime;

	public static LevelManager Instance 
	{
		get;
		private set;
	}

	public static string levelToLoad;

	public static void SetGravity(Vector3 gravity)
	{
		Physics.gravity = gravity;
	}
	
	private static void CheckMinYPosition()
	{
		for( int i = 0; i < Instance.MovingEntities.Count; i++ )
		{
			if(Instance.MovingEntities[i] != null && Instance.MovingEntities[i].transform.position.y < Instance.m_minYPostion)
			{
				Instance.MovingEntities[i].OnDie();
			}
		}
	}

	public List<MovingEntity> MovingEntities
	{
		get;
		set;
	}

	public List<HealthEntity> HealthEntities
	{
		get;
		set;
	}

	public static void AddMovingEntity(MovingEntity entity)
	{
		Instance.MovingEntities.Add (entity);
	}

	public static void RemoveMovingEntity(MovingEntity entity)
	{
		Instance.MovingEntities.Remove (entity);
	}

	public static void AddHealthEntity(HealthEntity entity)
	{
		Instance.HealthEntities.Add (entity);
	}
	
	public static void RemoveHealthEntity(HealthEntity entity)
	{
		Instance.HealthEntities.Remove (entity);
	}
	
	public static void RemoveEntity(EntityBase entity)
	{
		if (Instance.HealthEntities.Contains(entity as HealthEntity))
		{
			Instance.HealthEntities.Remove(entity as HealthEntity);
		}
		
		if (Instance.MovingEntities.Contains(entity as MovingEntity))
		{
			Instance.MovingEntities.Remove(entity as MovingEntity);
		}
	}

	[SerializeField]
	private float m_minYPostion;

	[SerializeField]
	private Vector3 m_gravity;

	public static bool AllowPlayerTeleport 
	{
		get;
		set;
	}

	private void Awake()
	{
		Instance = this;

		if(Instance != null)
		{
			if(UpgradeSystem.playerUpgrades.Count == 0 || UpgradeSystem.weaponUpgrades.Count == 0)
			{
				UpgradeSystem.InitUpgrades();
			}

			Damage.damageMultiplier = 1;

//			if( PerkSystem.AllPerks.Count == 0 )
//			{
//				PerkSystem.InitList();
//			}

			if(SaveManager.CurSaveData != null)
			{
				LoadGame();
				SaveGame();
				SetUpDifficulty();
				UIHandler.SetUpHealths();
			}

			GetAllMovingEntities ();
			GetAllHealthEntities();

			if(m_gravity != Vector3.zero)
			{
				LevelManager.SetGravity(m_gravity);
			}

			AllowPlayerTeleport = true;

			if(GameResources.CurrentPlayer.UseStealth)
			{
				UpgradeSystem.CurPlayerUpgrade.DoUpgrade();
			}
		}
	}

	private void SetUpDifficulty()
	{
		switch(DifficultyLevel)
		{
		case 1:
			GameResources.CurrentPlayer.SetUpHealth(3);
			break;
		case 2:
			GameResources.CurrentPlayer.SetUpHealth(2);
			break;
		case 3:
			GameResources.CurrentPlayer.SetUpHealth(1);
			break;
		}
	}

	private void GetAllMovingEntities()
	{
		Instance.MovingEntities = new List<MovingEntity> ();
		MovingEntity[] entities = FindObjectsOfType<MovingEntity> ();

		if(Instance.MovingEntities.Count > 0)
		{
			Instance.MovingEntities.Clear();
		}
		
		for (int i = 0; i < entities.Length; i++) 
		{
			Instance.MovingEntities.Add(entities[i]);
		}
	}

	private void GetAllHealthEntities()
	{
		Instance.HealthEntities = new List<HealthEntity> ();
		HealthEntity[] entities = FindObjectsOfType<HealthEntity> ();
		
		if(Instance.HealthEntities.Count > 0)
		{
			Instance.HealthEntities.Clear();
		}
		
		for (int i = 0; i < entities.Length; i++) 
		{
			Instance.HealthEntities.Add(entities[i]);
		}
	}

	private void FixedUpdate()
	{
		totalTime += Time.deltaTime;

		if(Instance != null)
		{
			LevelManager.CheckMinYPosition ();
		}
	}

	public void LoadGame()
	{
		totalTime = SaveManager.CurSaveData.time;
		LevelManager.DifficultyLevel = SaveManager.CurSaveData.difficulty;
		Vector3 savedPos = Vector3.zero;
		savedPos.x = SaveManager.CurSaveData.xPos;
		savedPos.y = SaveManager.CurSaveData.yPos;
		savedPos.z = SaveManager.CurSaveData.zPos;

		if(savedPos.x != 0 && savedPos.y != 0 && savedPos.z != 0 && Application.loadedLevelName == SaveManager.CurSaveData.currentLevel)
		{
			GameResources.CurrentPlayer.transform.position = savedPos;
		}

		GameResources.Currency = SaveManager.CurSaveData.currency;

		GameResources.CurrentPlayer.ownedPerks.Clear ();
		GameResources.CurrentPlayer.ownedUpgrades.Clear ();

		foreach(string name in SaveManager.CurSaveData.ownedPerks)
		{
			if( name != "")
				GameResources.CurrentPlayer.ownedPerks.Add(PerkSystem.GetPerkByName ( name ));
		}

		foreach(string name in SaveManager.CurSaveData.ownedUpgrades)
		{
			if( name != "")
				GameResources.CurrentPlayer.ownedUpgrades.Add(UpgradeSystem.GetUpgradeByName ( name ));
		}

		Debug.Log(SaveManager.CurSaveData.curWeaponPerk);
		Debug.Log(SaveManager.CurSaveData.curPlayerPerk);

//		UpgradeSystem.CurWeaponUpgrade = UpgradeSystem.GetUpgradeByName (SaveManager.CurSaveData.curWeaponPerk) as WeaponUpgrade;
//		UpgradeSystem.CurPlayerUpgrade = UpgradeSystem.GetUpgradeByName (SaveManager.CurSaveData.curPlayerPerk) as PlayerUpgrade;

		UpgradeSystem.ChangePlayerUpgrade(SaveManager.CurSaveData.curPlayerPerk);
		UpgradeSystem.ChangeWeaponUpgrade(SaveManager.CurSaveData.curWeaponPerk);


	}

	public void SaveGame()
	{
		SaveManager.SaveGame ();
	}

	public void ChangeLevel(string lvlName)
	{
		SaveGame ();
		Application.LoadLevel (lvlName);
	}

	public void ChangeLevel()
	{
		SaveGame ();
		Application.LoadLevel (LevelManager.levelToLoad);
	}

	public static void RestartLevel()
	{
		Application.LoadLevel (Application.loadedLevel);
	}
}