﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour 
{
	[SerializeField]
	private UnityEngine.UI.Button[] m_saveButtons;

	public SaveData[] datas;

	[SerializeField]
	private Slider m_difficultySlider;

	[SerializeField]
	private Text m_difficultyText;

	private CameraGlide m_cameraGlide;

	public static void LoadLevelWithName(string lvlName)
	{
		Application.LoadLevel (lvlName);
	}

	public static void LoadLevelWithId(int levelId)
	{
		Application.LoadLevel (levelId);
	}

	public void LoadSaveGame(int id)
	{
		SaveManager.CurSaveData = SaveManager.AllSaveDatas [id];

		if(SaveManager.CurSaveData.currentLevel == "")
		{
			m_cameraGlide.ShowDifficultySelection();
		}
		else
		{
			LoadLevelWithName(SaveManager.CurSaveData.currentLevel);
		}
	}

	private void InitSaveFiles()
	{
		for( int i = 0; i < m_saveButtons.Length; i++ )
		{
			for( int j = 0; j < SaveManager.AllSaveDatas.Count; j++ )
			{
				if(SaveManager.AllSaveDatas[i].currentLevel != "")
				{
					string difficulty = "";

					switch(SaveManager.AllSaveDatas[i].difficulty)
					{
					case 1:
						difficulty = "Easy";
						break;
					case 2:
						difficulty = "Normal";
						break;
					case 3:
						difficulty = "Hard";
						break;
					}
					m_saveButtons[i].GetComponentInChildren<Text>().text = "<size=30>Continue</size>\nLevel: "+SaveManager.AllSaveDatas[i].currentLevel +"\nDifficulty: " + difficulty + "\nTime: "+ GetTime(SaveManager.AllSaveDatas[i].time);
				}
				else
				{
					m_saveButtons[i].GetComponentInChildren<Text>().text = "New Game";
				}
			}
		}
	}

	private string GetTime(float seconds)
	{
		float m = seconds / 60;
		float h = m / 60;

		string minutes = Mathf.Round (m).ToString ();
		string hours = Mathf.Round(h).ToString();

		if(m < 10)
		{
			minutes = "0"+minutes;
		}

		if(h < 10)
		{
			hours = "0"+hours;
		}

		return hours + ":" + minutes;
	}

	public void StartNewGame()
	{
		SaveManager.CurSaveData.currentLevel = "Tutorial";
		LoadLevelWithName ("Tutorial");
	}

	public void ChangeDifficulty()
	{
		switch((int)m_difficultySlider.value)
		{
		case 1:
			m_difficultyText.text = "Easy";
			break;
		case 2:
			m_difficultyText.text = "Normal";
			break;
		case 3:
			m_difficultyText.text = "Hard";
			break;
		}

		SaveManager.CurSaveData.difficulty = (int)m_difficultySlider.value;
	}

	private void Awake()
	{
		m_cameraGlide = FindObjectOfType<CameraGlide> ();
		SaveManager.InitSaveDatas ();
		InitSaveFiles ();
		datas = SaveManager.AllSaveDatas.ToArray ();
		UpgradeSystem.InitUpgrades ();
	}
}