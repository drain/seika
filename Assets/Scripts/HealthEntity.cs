using UnityEngine;
using System.Collections;

public class HealthEntity : EntityBase, IDamage 
{
	[SerializeField]
	private int m_maxHealth;

	[SerializeField]
	private int m_currentHealth;

	public int CurrentHealth
	{
		get
		{
			return m_currentHealth;
		}
		
		protected set
		{
			m_currentHealth = Mathf.Min(value, m_maxHealth);
		}
	}

	public void SetUpHealth(int hAmount)
	{
		CurrentHealth = hAmount;
		MaxHealth = hAmount;
	}

	[SerializeField]
	private bool m_applyForceOnDamage;

	[SerializeField]
	private float m_force;

	public float Force 
	{
		get
		{
			return m_force;
		}
		set
		{
			m_force = value;
		}
	}
	
	[SerializeField]
	private bool m_dropsLoot;

	[SerializeField]
	private bool m_immuneAftherHit;

	[SerializeField]
	private float m_immuneAftherHitTime;

	private Coroutine m_waitForImmune;

	public int MaxHealth
	{
		get
		{
			return m_maxHealth;
		}
		
		protected set
		{
			m_currentHealth = m_maxHealth = value;
		}
	}
	
	public bool IsAlive
	{
		get
		{
			return m_currentHealth > 0;
		}
	}
	
	
	public virtual bool OnTakeDamage(Damage damage, Vector3 hitPosition)
	{
		if(m_waitForImmune == null && (transform.position - hitPosition).sqrMagnitude < 10)
		{
			DoDamage(damage.GetSetDamage);
			
			if(m_applyForceOnDamage)
			{
				GetComponent<Rigidbody> ().AddForce (-(hitPosition - transform.position).normalized * m_force, ForceMode.Impulse);
			}
			
			if(m_immuneAftherHit)
			{
				m_waitForImmune = StartCoroutine("WaitImmune");
			}
		}
		
		return true;
	}

	private IEnumerator WaitImmune()
	{
		//show visual?

		yield return new WaitForSeconds (m_immuneAftherHitTime);

		m_waitForImmune = null;

	}
	
	public virtual void DoDamage(int damage)
	{
		m_currentHealth -= Mathf.Abs(damage);
		
		if (!IsAlive)
		{
			OnDie();
		}
	}
	
	public virtual void DoHeal(int heal)
	{
		m_currentHealth = Mathf.Min(m_currentHealth + Mathf.Abs(heal), m_maxHealth);
	}
	
	protected override void Init ()
	{
		m_currentHealth = m_maxHealth;
		base.Init ();
	}
	
	public virtual void OnDie()
	{
		if(m_dropsLoot && m_entityType != EntityType.Player)
		{
			GetComponent<DropPickup>().DropItems();
		}
	}
}
