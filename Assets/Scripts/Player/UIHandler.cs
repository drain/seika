﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour 
{
	public static UIHandler Instance;

	public Image[] healthBarImages;

	[SerializeField]
	private Text m_currency;

	[SerializeField]
	private Image[] m_healths;
	
	public GameObject saveButton;

	public UnityEngine.UI.Button changeLevelButton;

	[SerializeField]
	private UnityEngine.UI.Button[] m_perkSelectButtons;
	
	private List<int> m_perkButtonIds = new List<int>();

	private float m_pickupCounter;

	[SerializeField]
	private float m_waitForAnotherPickup;

	private Coroutine m_onGoingCoroutine;

	private float m_pickedAmount;

	private float m_oldPickedAmount;

	[SerializeField]
	private float m_showValueOnly;

	private float m_showTime;

	[SerializeField]
	private bool m_usePerks;

	[SerializeField]
	private GameObject m_perkSelection;

	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		if(Instance != null)
		{
			Instance.m_currency.text = "Currency: " + GameResources.Currency.ToString ();

			if(m_usePerks && m_perkSelection != null)
			{
				PerkSystem.InitList();
				GameResources.IsPaused = true;
				InitPerkButtons();
			}
			else if(!m_usePerks && m_perkSelection != null)
			{
				m_perkSelection.SetActive(false);
			}

			if(saveButton != null)
			{
				saveButton.gameObject.SetActive(false);
			}

			if(changeLevelButton != null)
			{
				changeLevelButton.gameObject.SetActive(false);
			}
		}
	}

	public void InitPerkButtons()
	{
		for(int i = 0; i < m_perkSelectButtons.Length; i++)
		{
			Perk temp = PerkSystem.AllPerks[Random.Range(0,PerkSystem.AllPerks.Count)];

			if(m_perkButtonIds.Contains(temp.perkId))
			{
				i--;
				continue;
			}

			m_perkSelectButtons[i].GetComponentInChildren<Text>().text = temp.perkName;

			m_perkSelectButtons[i].onClick.AddListener(() => { 

				SelectPerk(temp.perkId);
			});

			m_perkButtonIds.Add(temp.perkId);
		}
	}

	public void SelectPerk(int perkId)
	{
		GameResources.IsPaused = false;
		PerkSystem.GetPerkById (perkId).DoPerk();
		m_perkSelection.SetActive (false);
	}

	public void DontUsePerk()
	{
		GameResources.IsPaused = false;
		m_perkSelection.SetActive (false);
	}
	
	public static void UpdateCurrencyText(float pickedAmount)
	{
		if(Instance == null)
		{
			return;
		}

		Instance.m_pickupCounter = 0;

		if(Instance.m_onGoingCoroutine == null)
		{
			Instance.m_pickedAmount += pickedAmount;

			Instance.m_onGoingCoroutine = Instance.StartCoroutine (Instance.UpdateScoreText());
			Instance.StartCoroutine(Instance.ScaleText(Instance.m_currency.rectTransform));
		}
		else
		{
			Instance.m_pickedAmount += pickedAmount;
			Instance.StartCoroutine(Instance.ScaleText(Instance.m_currency.rectTransform));
		}
	}

	private IEnumerator ScaleText(RectTransform transformToScale)
	{
		transformToScale.localScale *= 1.15f;
		yield return new WaitForSeconds (0.25f);
		transformToScale.localScale /= 1.15f;
	}

	private IEnumerator UpdateScoreText()
	{
		float time = 0;
		float alpha = 0;
		Color temp = m_currency.color;

		alpha = 1;
		temp.a = alpha;
		m_currency.color = temp;

		while(m_pickupCounter < m_waitForAnotherPickup)
		{
			m_currency.text = "Currency: " + (GameResources.Currency - m_pickedAmount).ToString() + "<color=green> + " + m_pickedAmount.ToString() + "</color>";
			m_pickupCounter += Time.deltaTime;
			yield return null;
		}

		m_oldPickedAmount = m_pickedAmount;
		m_currency.text = "Currency: " + GameResources.Currency;

		m_showTime = 0;
		while(m_showTime < m_showValueOnly)
		{
			if(m_pickedAmount != m_oldPickedAmount)
			{
				m_pickedAmount = m_pickedAmount - m_oldPickedAmount;
				m_oldPickedAmount = 0;
				m_showTime = 0;

				m_onGoingCoroutine = null;

				Instance.m_onGoingCoroutine = Instance.StartCoroutine (Instance.UpdateScoreText());
				yield break;
			}

			m_showTime += Time.deltaTime;
			yield return null;
		}

		while(alpha > 0)
		{
			alpha = Mathf.Lerp (m_currency.color.a, 0, time);
			temp = m_currency.color;
			temp.a = alpha;
			m_currency.color = temp;
			time += Time.deltaTime * 2;
			yield return null;
		}

		m_pickedAmount = 0;
		m_onGoingCoroutine = null;
	}

	public static void SetUpHealths ()
	{
		if(Instance != null)
		{
			foreach(Image health in Instance.m_healths)
			{
				health.gameObject.SetActive(false);
			}

			switch(LevelManager.DifficultyLevel)
			{
			case 1:
				UpdateHealths(3);
				break;
			case 2:
				UpdateHealths(2);
				break;
			case 3:
				UpdateHealths(1);
				break;
			}
		}
	}

	public static void UpdateHealths(float currentHealth)
	{
		if(Instance != null)
		{
			for( int i = 0; i < Instance.m_healths.Length; i++ )
			{
				Instance.StartCoroutine(Instance.ScaleText(Instance.m_healths[i].rectTransform));
				if(i + 1 > currentHealth)
				{
					Instance.m_healths[i].gameObject.SetActive(false);
				}
				else if(i + 1 <= GameResources.CurrentPlayer.MaxHealth)
				{
					Instance.m_healths[i].gameObject.SetActive(true);
				}
			}
		}
	}
}