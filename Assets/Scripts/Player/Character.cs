﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Character : MovingEntity {

	public float attackTime;

	private Vector3 m_mousePos;

	public bool IsDoingAttack
	{
		get
		{
			return m_anim.GetCurrentAnimatorStateInfo(0).IsTag("Attack");
		}
	}
	
	public bool IsDoingBlock
	{
		get
		{
			return m_anim.GetCurrentAnimatorStateInfo(0).IsTag("Block");
		}
	}

	private Quaternion m_oldRotation;

	private Transform m_cameraTransform;
	private float m_horAxis;
	private float m_verAxis;
	private Vector3 m_move;

	private Animator m_anim;

	public float teleportDistance;
	public float teleportTime;

	private bool m_allowTeleport;
	private bool m_allowJump;
	private bool m_allowAttack;

	[SerializeField]
	private LayerMask m_teleportMask;

	[SerializeField]
	private ParticleSystem m_teleportParticles;

	private float m_jumpAnimDelay;

	[SerializeField]
	private float m_maxJumpAnimDelay;

	[SerializeField]
	private Transform m_playerCursor;

	[SerializeField]
	private Renderer m_bodyMesh;

	[SerializeField]
	private Renderer m_hairMesh;

	public bool useMouseTeleport;

	public List<Perk> ownedPerks;
	public List<Upgrade> ownedUpgrades;

	private Transform m_curDragObject;

	private Transform m_collideredObject;

	public Vector3 GetCurrentMoveDirection()
	{
		return m_move;
	}

	public bool IsDraggingObject()
	{
		if(m_curDragObject != null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	private bool m_playFallingAnim;
	
	public bool UseStealth
	{
		get
		{
			return m_useStealth;
		}
		set
		{
			m_useStealth = value;
		}
	}

	public bool DashDamage
	{
		get
		{
			return m_dashDamage;
		}
		set
		{
			m_dashDamage = value;
		}
	}

	public bool UseSharpWeapon
	{
		get
		{
			return m_useSharpWeapon;
		}
		set
		{
			m_useSharpWeapon = value;
		}
	}

	private bool m_useStealth;

	private bool m_dashDamage;

	private bool m_useSharpWeapon;

	private int m_phoenixUpgradeLives = 1;
	
	//this is to make sure that we don't play falling animation, if we only fall a little bit.
	protected override void OnCollisionStay(Collision col)
	{
		base.OnCollisionStay (col);

		if(col.contacts.Length > 0)
		{
			ContactPoint contact = col.contacts[0];
			
			float length = Mathf.Round(contact.normal.x + contact.normal.z);
			if ( length == 0 && contact.normal.y - 1f >= 0)
			{
				m_playFallingAnim = true;
			}
			else
			{
				m_playFallingAnim = false;
			}
		}
	}

	protected void OnCollisionEnter(Collision col)
	{
		if(col.collider.gameObject.layer == 10 && !IsDraggingObject())
		{
			m_collideredObject = col.collider.transform;
		}
	}

	protected override void OnCollisionExit (Collision col)
	{
		base.OnCollisionExit (col);

		if(col.collider.gameObject.layer == 10)
		{
			m_collideredObject = null;
		}
	}

	private Vector3 MouseDir()
	{
		m_mousePos.x = Input.mousePosition.x;
		m_mousePos.y = Input.mousePosition.y;
		m_mousePos.z = Camera.main.WorldToScreenPoint(m_transform.position).z;

		return Camera.main.ScreenToWorldPoint(m_mousePos);
	}

	protected override void Init()
	{
		base.Init ();
		GameResources.CurrentPlayer = this;

		m_cameraTransform = Camera.main.transform;

		m_anim = GetComponent<Animator> ();

		m_allowTeleport = true;

		m_allowAttack = true;

		m_allowJump = true;

		m_jumpAnimDelay = m_maxJumpAnimDelay;

		UIHandler.UpdateHealths (CurrentHealth);
	}

	protected override void DoFixedUpdate()
	{
		if(!IsDoingAttack && m_allowMovement)
		{
			MovePlayer();
		}
	}

	protected override void DoUpdate()
	{
		if(Input.GetButtonDown("Jump") && IsGrounded() && m_allowJump)
		{
			Jump(m_move);

			if(m_anim != null)
			{
				m_anim.SetTrigger("DoJump");
				
				//starts jumpdelay
				m_jumpAnimDelay = 0;			
			}
		}

		if(m_anim != null)
		{
			if(m_jumpAnimDelay < m_maxJumpAnimDelay)
			{
				m_jumpAnimDelay += Time.deltaTime;
				m_anim.SetBool("IsJumping",true);
			}
			else
			{
				m_anim.SetBool("IsJumping",false);

				if(!IsGrounded() && m_playFallingAnim)
				{
					m_anim.SetBool("Falling",true);
				}
				else
				{
					m_anim.SetBool("Falling",false);
				}
			}
		}
		
		if(Input.GetMouseButtonDown(0) && m_allowAttack)
		{
			m_anim.SetTrigger("Attack");

			//start attack
			if(!IsDoingAttack)		
			{
				m_anim.SetTrigger("DoAttack");
				DoingAttack(MouseDir());

				if(UpgradeSystem.CurWeaponUpgrade != null && UpgradeSystem.CurWeaponUpgrade.upgradeName == "Shoot Projectiles")
				{
					GetComponent<EnemyProjectile>().DoAttack(this,null,"");
				}
			}
		}
		
		if(Input.GetMouseButtonDown(1) &&  m_allowAttack && !IsDoingBlock)
		{
			DoingBlock(MouseDir());
		}

		if(m_playerCursor != null)
		{
			SetPlayerCursor ();
		}
		
		if(Input.GetKeyDown(KeyCode.E))
		{
			Teleport();
		}

		if(Input.GetKeyDown(KeyCode.G) && !IsDraggingObject() && m_collideredObject != null)
		{
			StartDragging(m_collideredObject);
		}
		else if(Input.GetKeyDown(KeyCode.G) && IsDraggingObject())
		{
			EndDragging();
		}
	}

	private void StartDragging(Transform drag)
	{
		m_curDragObject = drag;
		m_curDragObject.SetParent (m_transform);

		Destroy (m_curDragObject.GetComponent<Rigidbody> ());
		m_collideredObject = null;

		moveSpeed /= 2;
		m_allowTeleport = false;
		m_allowJump = false;
		m_allowAttack = false;

	}

	private void EndDragging()
	{
		m_allowTeleport = true;
		m_allowJump = true;
		m_allowAttack = true;
		moveSpeed *= 2;

		m_curDragObject.gameObject.AddComponent<Rigidbody> ();
		m_curDragObject.parent = null;
		m_curDragObject = null;
	}

	private void MovePlayer()
	{
		m_oldRotation = m_cameraTransform.rotation;

		Vector3 temp = m_oldRotation.eulerAngles;
		temp.x = 0;
		m_cameraTransform.rotation = Quaternion.Euler(temp);
		
		m_horAxis = Input.GetAxis("Horizontal");
		m_verAxis = Input.GetAxis("Vertical");

		m_move.x = m_horAxis;
		m_move.y = 0;
		m_move.z = m_verAxis;

		m_move = m_cameraTransform.TransformDirection(m_move);
		
		m_cameraTransform.rotation = m_oldRotation;
		
		m_move.y = 0;
		
		if (Mathf.Abs(m_verAxis) > 0.15f || Mathf.Abs(m_horAxis) > 0.15f)
		{
			Rotate(m_move);
		}
		
		if (m_anim.GetNextAnimatorStateInfo(0).IsTag("Moving") || m_anim.GetCurrentAnimatorStateInfo(0).IsTag("Moving"))
		{
			Move (m_move);
		}
	}

	protected override void Update()
	{
		base.Update ();

		if(m_anim != null)
		{
			if( (Mathf.Abs(m_move.x) > 0 || Mathf.Abs(m_move.z) > 0) && !GameResources.IsPaused)
			{
				m_anim.SetBool ("Moving", true);
			}
			else
			{
				m_anim.SetBool ("Moving", false);
			}
		}
	}

	protected void Teleport()
	{
		if(m_allowTeleport && !IsDoingAttack && LevelManager.AllowPlayerTeleport)
		{
			if(m_anim != null)
			{
				m_anim.SetTrigger("Teleport");
			}

			if(useMouseTeleport)
			{
				StartCoroutine("DoMouseTeleport");
			}
			else
			{
				StartCoroutine("DoTeleport");
			}
		}
	}

	private IEnumerator DoTeleport()
	{
		m_allowTeleport = false;

		if(m_teleportParticles != null)
		{
			m_teleportParticles.Play ();
		}

		RaycastHit[] hit = Physics.RaycastAll(m_transform.position - m_transform.forward,m_transform.forward,teleportDistance + 1,m_teleportMask);

		foreach(RaycastHit h in hit)
		{
			Debug.Log(h.collider.name);
		}
		
		if (hit.Length > 1 && hit[1].collider != null)
		{
			if(DashDamage)
			{
				EnemyAI ea = hit[1].collider.GetComponent<EnemyAI>();
					
				if(ea != null)
				{
					ea.DoDamage(1);

				}
			}

			m_transform.position = hit[1].point - m_transform.forward;
		}
		else
		{
			m_transform.Translate (m_transform.forward * teleportDistance,Space.World);
		}
		
		yield return new WaitForSeconds (teleportTime);

		m_allowTeleport = true;
	}

	private IEnumerator DoMouseTeleport()
	{
		Vector3 rotateDir = MouseDir () - m_transform.position;
		rotateDir.y = 0;

		m_allowTeleport = false;
		
		if(m_teleportParticles != null)
		{
			m_teleportParticles.Play ();
		}
		
		RaycastHit hit;
		
		if(Physics.Raycast(m_transform.position - m_transform.forward,rotateDir,out hit,teleportDistance + 1,m_teleportMask))
		{
			Rotate(rotateDir);
			m_transform.position = hit.point - m_transform.forward;
		}
		else
		{
			Rotate(rotateDir);
			m_transform.Translate (m_transform.forward * teleportDistance,Space.World);
		}
		
		yield return new WaitForSeconds (teleportTime);
		m_allowTeleport = true;
	}

	private void DoingAttack(Vector3 direction)
	{
		m_anim.SetBool("Moving",false);
	
		Vector3 dir = direction - m_transform.position;
		dir.y = 0;
		m_transform.rotation = Quaternion.FromToRotation (Vector3.forward, dir.normalized);
		
		Rotate (dir);
	}
	
	private void DoingBlock(Vector3 direction)
	{
		m_anim.SetTrigger("DoBlock");
		
		Vector3 dir = direction - m_transform.position;
		dir.y = 0;
		m_transform.rotation = Quaternion.FromToRotation (Vector3.forward, dir.normalized);
		
		Rotate (dir);
	}

	private void SetPlayerCursor()
	{
		Vector3 lookAtDir = MouseDir ();
		lookAtDir.y = -90f;
		
		m_playerCursor.LookAt(lookAtDir);
	}

	public override void DoDamage(int damage)
	{
		base.DoDamage (damage);
		UIHandler.UpdateHealths (CurrentHealth);
	}
	
	public override void DoHeal(int heal)
	{
		base.DoHeal(heal);
		UIHandler.UpdateHealths (CurrentHealth);
	}

	public override void OnDie ()
	{
		base.OnDie ();

		if(UpgradeSystem.CurPlayerUpgrade != null && UpgradeSystem.CurPlayerUpgrade.GetType() == typeof(RespawnPlayer) && m_phoenixUpgradeLives > 0)
		{
			// Instead of hard coded logic => (Base Class method, Try/Do Upgade => PUT LOGIC HERE!) Or (Event based system).
			DoHeal(1);
			m_phoenixUpgradeLives -= 1;
			return;
		}

		LevelManager.RestartLevel ();
	}

	public void ChangeBodyColor(Color color)
	{
		m_bodyMesh.material.SetColor ("_EmissionColor", color * 5f);
	}

	public void ChangeHairColor(Color color)
	{
		m_hairMesh.material.SetColor ("_Color", color);
	}

	public void ChangeTeleportColor(Color color)
	{
		m_teleportParticles.startColor = color;
	}
	
	public override bool OnTakeDamage (Damage damage, Vector3 hitPosition)
	{
		Vector3 hitDir = (hitPosition - m_transform.position).normalized;
		hitDir.y = 0;

		float minDot = 0;
		
		if (damage.GetDamageType == Damage.DamageType.Melee)
		{
			minDot = -0.25f;
		}
		
		if (Vector3.Dot(m_transform.forward, hitDir) > minDot && IsDoingBlock)
		{
			damage.Entity = this;
			return false;
		}
		
		return base.OnTakeDamage (damage, hitPosition);
	}
}