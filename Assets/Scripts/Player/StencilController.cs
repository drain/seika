﻿using UnityEngine;
using System.Collections;

public class StencilController : MonoBehaviour 
{
	[SerializeField]
	private GameObject m_stencilMask;
	
	[SerializeField]
	private Transform m_target;
	
	[SerializeField]
	private bool m_hideRenderer;
	
	private new Transform transform;
	
	private void Awake()
	{
		transform = GetComponent<Transform>();
	}
	
	private void FixedUpdate()
	{
		if (m_target)
		{
			transform.LookAt(m_target.position, Vector3.up);
		}
	}
	
	private void OnTriggerExit(Collider collider)
	{
		if ( m_stencilMask)
		{
			m_stencilMask.SetActive(false);
			
			Renderer renderer = collider.gameObject.GetComponent<Renderer> ();
			if(renderer != null && CheckMaterials(renderer.sharedMaterials))
			{
				if (m_hideRenderer)
				{
					renderer.enabled = true;
				}
			}
		}
	}
	
	private void OnTriggerStay(Collider collider)
	{
		if (m_stencilMask)
		{
			Renderer renderer = collider.gameObject.GetComponent<Renderer> ();
			if(renderer != null && CheckMaterials(renderer.sharedMaterials))
			{
				if (m_hideRenderer)
				{
					renderer.enabled = false;
				}
				
				m_stencilMask.SetActive(true);
			}
		}
	}
	
	private bool CheckMaterials(Material[] materials)
	{
		for (int i = 0; i < materials.Length; i++)
		{
			if (materials[i].shader.name.Contains("Stencil"))
			{
				return true;
			}
		}
		
		return false;
	}
}