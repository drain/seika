﻿using UnityEngine;
using System.Collections;

public class CustomizeSystem : MonoBehaviour 
{
	private Weapon m_playerWeapon;

	private Character m_player;

	public Color defaultCharacterColor;
	public Color defaultHairColor;
	public Color defaultWeaponColor;

	private void Awake()
	{
		UpgradeSystem.customizeSystem = this;

		m_player = FindObjectOfType<Character> ();
		m_playerWeapon = m_player.GetComponentInChildren<Weapon> ();
	}

	private void Start()
	{
		// Otherwise throws null reference error! - Atte
		if(SaveManager.CurSaveData == null || SaveManager.CurSaveData.curWeaponPerk == "")
		{
			UpdateWeaponColor (defaultWeaponColor);
		}

		if(SaveManager.CurSaveData == null || SaveManager.CurSaveData.curPlayerPerk == "")
		{
			UpdateBodyColor (defaultCharacterColor);
		}
	

		UpdateHairColor (defaultHairColor);
	}

	public void UpdateWeaponColor(Color color)
	{
		m_playerWeapon.ChangeWeaponColor (color);
		ChangeHealtBarColor(color);
	}

	public void UpdateHairColor(Color color)
	{
		m_player.ChangeHairColor (color);
	}

	public void UpdateBodyColor(Color color)
	{
		m_player.ChangeBodyColor (color);
		m_player.ChangeTeleportColor (color);
	}

	public void ChangeHealtBarColor(Color color)
	{
		for( int i = 0; i < UIHandler.Instance.healthBarImages.Length; i++ )
		{
			UIHandler.Instance.healthBarImages[i].color = color;
		}
	}
}