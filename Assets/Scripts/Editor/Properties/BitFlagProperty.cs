﻿using System.Collections;
using UnityEditor;
using UnityEngine;
using Properties;

[CustomPropertyDrawer(typeof(BitFlag))]
public class GBitFlagProperty : PropertyDrawer
{
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
		property.intValue = EditorGUI.MaskField(position,label,property.intValue,property.enumNames);
	}
}