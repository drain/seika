﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour 
{
	public enum Type
	{
		Perk,
		Upgrade
	}

	[System.Serializable]
	public class PriceTable
	{
		public string name = "";
		public float price = 0;
		public Type type;
	}

	public List<PriceTable> priceTable;

	public PriceTable GetPricetableByname(string name)
	{
		for( int i = 0; i < priceTable.Count; i++ )
		{
			if(priceTable[i].name == name)
			{
				return priceTable[i];
			}
		}

		return null;
	}

	public void AddCurrency(int amount)
	{
		GameResources.Currency += amount;
	}

	[SerializeField]
	private GameObject m_shopUI;

	[SerializeField]
	private GameObject m_shop;

	[SerializeField]
	private GameObject m_upgradeSelection;

	[SerializeField]
	private GameObject m_perksObj;

	[SerializeField]
	private GameObject m_upgradesObj;

	public static GameObject tooltip;

	[SerializeField]
	private GameObject m_tooltip;

	[SerializeField]
	private Text m_currencyText;

	[SerializeField]
	private DraggableSlot[] m_shopBuyButtons;
	
	public DraggableSlot[] upgradeSelectionObjs;

	
	public void ChangeTooltipText(string name)
	{
		string tText = "";

		Upgrade tempU = UpgradeSystem.GetUpgradeByName (name);
		Perk tempP = PerkSystem.GetPerkByName (name);
		
		if (tempU != null) 
		{
			tText += tempU.desc + "\n\nPrice: " +  GetPricetableByname( name ).price;
		}
		else if(tempP != null)
		{
			tText += tempP.desc + "\n\nPrice: " +  GetPricetableByname( name ).price;
		}

		tooltip.GetComponentInChildren<Text> ().text = tText;
	}
	
	public void ToggleShop()
	{
		m_shop.SetActive (true);
		m_upgradeSelection.SetActive (false);
	}

	public void ToggleUpgradeSelection()
	{
		m_shop.SetActive (false);
		m_upgradeSelection.SetActive (true);
	}

	public void ToggleShopPerks()
	{
		m_perksObj.SetActive (true);
		m_upgradesObj.SetActive (false);
	}
	
	public void ToggleShopUpgrades()
	{
		m_perksObj.SetActive (false);
		m_upgradesObj.SetActive (true);
	}

	public void ShowTooltip()
	{
		tooltip.SetActive (true);

	}

	public void StartShop()
	{
		m_shopUI.SetActive (true);
	}
	
	public void CloseShop()
	{
		m_shopUI.SetActive (false);
	}

	public void Start()
	{
		Init ();
		tooltip = m_tooltip;
		m_tooltip.SetActive (false);
		m_perksObj.SetActive (false);
		m_upgradesObj.SetActive (false);
		m_shop.SetActive (true);
		m_upgradeSelection.SetActive (false);

	}
	
	public void Init()
	{
		for( int i = 0; i < m_shopBuyButtons.Length; i++ )
		{
			if(GameResources.CurrentPlayer.ownedPerks.Contains( PerkSystem.GetPerkByName( m_shopBuyButtons[i].itemName )) || 
			   GameResources.CurrentPlayer.ownedUpgrades.Contains( UpgradeSystem.GetUpgradeByName( m_shopBuyButtons[i].itemName )))
			{
				m_shopBuyButtons[i].GetComponent<UnityEngine.UI.Button>().enabled = false;
				m_shopBuyButtons[i].GetComponent<Image>().color = Color.gray;

				if(m_shopBuyButtons[i].GetComponentInChildren<Text>())
					m_shopBuyButtons[i].GetComponentInChildren<Text>().text = "Bought";
			}
		}

		for( int i = 0; i < upgradeSelectionObjs.Length; i++ )
		{
			if( !GameResources.CurrentPlayer.ownedUpgrades.Contains( UpgradeSystem.GetUpgradeByName( upgradeSelectionObjs[i].itemName )))
			{
				upgradeSelectionObjs[i].GetComponent<Image>().color = Color.gray;
				upgradeSelectionObjs[i].allowDrag = false;
			}
			else
			{
				upgradeSelectionObjs[i].GetComponent<Image>().color = Color.white;
				upgradeSelectionObjs[i].allowDrag = true;
			}
		}

		//SaveManager.SaveGame();
	}

	private void FixedUpdate()
	{
		m_currencyText.text = "Currency: " + GameResources.Currency;
	}

	public void Purchase(string name)
	{
		PriceTable table = GetPricetableByname(name);

		if(table != null)
		{
			Type type = table.type;

			if(type == Type.Perk)
			{
				Perk perkToBuy = PerkSystem.GetPerkByName (name);
				
				if(perkToBuy != null )
				{
					if((GameResources.Currency - table.price) >= 0  && !GameResources.CurrentPlayer.ownedPerks.Contains(perkToBuy))
					{
						GameResources.Currency -= table.price;
						GameResources.CurrentPlayer.ownedPerks.Add(perkToBuy);
						SaveManager.SaveGame();

						Init();
					}
				}
				
			}
			else if(type == Type.Upgrade)
			{
				Upgrade upgradeToBuy = UpgradeSystem.GetUpgradeByName (name);

				if(upgradeToBuy != null)
				{
					if((GameResources.Currency - table.price) >= 0 && !GameResources.CurrentPlayer.ownedUpgrades.Contains(upgradeToBuy))
					{
						GameResources.Currency -= table.price;
						GameResources.CurrentPlayer.ownedUpgrades.Add(upgradeToBuy);
						SaveManager.SaveGame();
						
						Init();
					}
				}
			}
		}
	}
}