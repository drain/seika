﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class DraggableSlot : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler , IPointerEnterHandler, IPointerExitHandler
{
	public bool allowDrag;

	public string itemName = "";
	public static DraggableSlot curDragSlot = null;

	public Image img;

	private RectTransform m_transform;
	
	private Vector3 m_startPos;

	private Transform m_oldParent;

	[SerializeField]
	private Transform m_dragTrans;

	private ShopManager m_shopManager;

	private void Awake()
	{
		m_transform = GetComponent<RectTransform>();
		m_startPos = m_transform.localPosition;
		img = GetComponent<Image> ();
		m_oldParent = m_transform.parent;
		m_shopManager = FindObjectOfType<ShopManager> ();
	}

	#region IBeginDragHandler implementation

	public void OnBeginDrag (PointerEventData eventData)
	{
		if(allowDrag)
		{
			curDragSlot = this;
			curDragSlot.m_transform.SetParent (m_dragTrans);
			curDragSlot.GetComponent<CanvasGroup>().blocksRaycasts = false;
		}
	}

	#endregion
	
	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{
		if(allowDrag)
		{
			curDragSlot.transform.position = Input.mousePosition;
		}
	}

	#endregion
	
	#region IEndDragHandler implementation
	public void OnEndDrag (PointerEventData eventData)
	{
		if(allowDrag)
		{
			curDragSlot.m_transform.SetParent (m_oldParent);
			curDragSlot.m_transform.localPosition = m_startPos;
			curDragSlot.GetComponent<CanvasGroup>().blocksRaycasts = true;
			curDragSlot = null;
		}
	}
	#endregion

	#region IPointerEnterHandler implementation

	public void OnPointerEnter (PointerEventData eventData)
	{
		ShopManager.tooltip.SetActive (true);

		m_shopManager.ChangeTooltipText( itemName );
	}

	#endregion

	#region IPointerExitHandler implementation

	public void OnPointerExit (PointerEventData eventData)
	{

		ShopManager.tooltip.SetActive (false);
	}

	#endregion
}