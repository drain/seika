using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DropSlot : MonoBehaviour, IDropHandler, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
	public string curUpgradeName = "";

	public Upgrade.UpradeType type;

	private ShopManager m_shopManager;

	private Image m_image;
	private Sprite m_noneImg;

	private void Awake()
	{
		m_shopManager = FindObjectOfType<ShopManager> ();
		m_image = GetComponent<Image> ();
		m_noneImg = m_image.sprite;

		Init ();
	}

	private void Init()
	{
		if(SaveManager.CurSaveData != null)
		{
			if(type == Upgrade.UpradeType.Weapon)
			{
				curUpgradeName = SaveManager.CurSaveData.curWeaponPerk;
			}
			else if(type == Upgrade.UpradeType.Player)
			{
				curUpgradeName = SaveManager.CurSaveData.curPlayerPerk;
			}
			
			foreach(DraggableSlot dSlot in m_shopManager.upgradeSelectionObjs)
			{
				if( curUpgradeName == dSlot.itemName )
				{
					m_image.sprite = dSlot.img.sprite;
				}
			}
		}

		SetUpgradeToSlot ();
	}

	private void SetUpgradeToSlot()
	{
		Upgrade upgrade = UpgradeSystem.GetUpgradeByName( type, curUpgradeName );
		
		if( upgrade != null && type == upgrade.upgradeType )
		{
			if(type == Upgrade.UpradeType.Weapon)
			{
				UpgradeSystem.ChangeWeaponUpgrade(curUpgradeName);
				SetImage();
			}
			else if (type == Upgrade.UpradeType.Player)
			{
				UpgradeSystem.ChangePlayerUpgrade(curUpgradeName);
				SetImage();
			}
		}
	}

	private void SetImage()
	{
		if(DraggableSlot.curDragSlot != null)
		{
			m_image.sprite = DraggableSlot.curDragSlot.img.sprite;
		}
	}

	#region IDropHandler implementation

	public void OnDrop (PointerEventData eventData)
	{
		curUpgradeName = DraggableSlot.curDragSlot.itemName;
		SetUpgradeToSlot ();
	}

	#endregion

	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData)
	{
		if(eventData.button == PointerEventData.InputButton.Right)
		{
			m_image.sprite = m_noneImg;
			curUpgradeName = "";

			if(type == Upgrade.UpradeType.Weapon)
			{
				UpgradeSystem.ChangeWeaponUpgrade("");
			}
			else if(type == Upgrade.UpradeType.Player)
			{
				UpgradeSystem.ChangePlayerUpgrade("");
			}
		}
	}
	#endregion

	#region IPointerEnterHandler implementation
	
	public void OnPointerEnter (PointerEventData eventData)
	{
		ShopManager.tooltip.SetActive (true);

		m_shopManager.ChangeTooltipText( curUpgradeName );
	}
	
	#endregion
	
	#region IPointerExitHandler implementation
	
	public void OnPointerExit (PointerEventData eventData)
	{
		
		ShopManager.tooltip.SetActive (false);
	}
	
	#endregion
}