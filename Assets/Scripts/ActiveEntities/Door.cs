﻿using UnityEngine;
using System.Collections;

public class Door : PuzzleObject
{
	[SerializeField]
	private bool m_openWithHit;

	[SerializeField]
	private bool m_useableMultipleTimes;

	private bool m_isTriggered;

	private Animator m_anim;

	protected override void OnMeleeHit (Damage damage)
	{
		if(m_openWithHit && !m_isTriggered)
		{
			OpenDoor();
		}
		else if(m_isTriggered && m_useableMultipleTimes)
		{
			CloseDoor();
		}
	}
	
	private void Awake()
	{
		m_anim = GetComponent<Animator> ();

		if(m_anim == null)
		{
			Debug.LogWarning("Door requires animator!");
		}
	}

	public void OpenDoor()
	{
		m_anim.SetTrigger("Open");
		m_isTriggered = true;
	}

	public void CloseDoor()
	{
		m_anim.SetTrigger("Close");
		m_isTriggered = false;
	}
}