﻿using UnityEngine;
using System.Collections;

public class MovingEntity : HealthEntity {

	public float moveSpeed;
	public float jumpForce;
	protected bool m_allowMovement;
	
	private bool m_isGrounded;
	private float m_lookAngle;

	protected Transform m_transform;
	protected Rigidbody m_rigidbody;
	
	public Transform GetTransform
	{
		get
		{
			return m_transform;
		}
	}

	public bool IsGrounded()
	{
		return m_isGrounded;
	}
	
	protected override void Init()
	{
		base.Init ();
		m_transform = GetComponent<Transform> ();
		m_rigidbody = GetComponent<Rigidbody> ();
		m_allowMovement = true;
	}

	protected override void Update ()
	{
		if (!GameResources.StopMovingEntities) 
		{
			base.Update ();
		}
	}

	protected override void FixedUpdate ()
	{
		if (!GameResources.StopMovingEntities) 
		{
			base.FixedUpdate ();
		}
	}

	protected void Move(Vector3 moveVector)
	{
		if(m_allowMovement)
		{
			m_transform.Translate (moveVector * Time.fixedDeltaTime * moveSpeed, Space.World);
		}
	}

	protected void Rotate(Vector3 rotateVector)
	{
		if(m_allowMovement)
		{
			m_transform.rotation = Quaternion.LookRotation(rotateVector);
		}
	}

	protected void Jump(Vector3 dir)
	{
		if(m_isGrounded)
		{
			m_transform.position += Vector3.up * 0.1f;

			m_rigidbody.AddForce (Vector3.up * jumpForce, ForceMode.Impulse);
			//m_allowMovement = false;
			m_isGrounded = false;
		}
	}

	protected virtual void OnCollisionStay(Collision col)
	{
		if(col.contacts.Length > 0)
		{
			ContactPoint contact = col.contacts[0];

			float length = Mathf.Round(contact.normal.x + contact.normal.z);
			if ( length == 0 && contact.normal.y >= 0)
			{
				m_isGrounded = true;
			}
		}
	}

	protected virtual void OnCollisionExit(Collision col)
	{
		m_isGrounded = false;
	}
}