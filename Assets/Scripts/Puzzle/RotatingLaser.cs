using UnityEngine;
using System.Collections;

public class RotatingLaser : Laser 
{
	[SerializeField]
	private float m_rotationSpeed;

	[SerializeField]
	private float m_angle;


	protected override void Awake ()
	{
		base.Awake ();
		StartCoroutine (LoopRotation (m_angle));
	}

	private IEnumerator LoopRotation(float angle)
	{
		float rot = 0f;
		float dir = 1f;

		while(true)
		{
			while(rot < angle)
			{
				float step = Time.deltaTime * m_rotationSpeed;
				transform.Rotate(Vector3.up * step * dir);
				rot += step;
				yield return null;
			}

			rot= 0f;
			dir *= -1f;
			yield return null;
		}
	}
}