using UnityEngine;
using System.Collections;

public class TimedPressurePlate : PressurePlate 
{
	private float m_timer;

	[SerializeField]
	private float m_maxTimer;

	[SerializeField]
	private Laser[] m_lasersToActive;

	[SerializeField]
	private bool m_DisableLaser;

	private bool m_doOnce = true;

	protected override void DoFixedUpdate ()
	{
		base.DoFixedUpdate ();

		if(m_timer < m_maxTimer)
		{
			m_timer += Time.deltaTime;
		}
		else
		{
			if(m_doOnce)
			{
				m_doOnce = false;

				for( int i = 0; i < m_lasersToActive.Length; i++ )
				{
					m_lasersToActive[i].SetActive(m_DisableLaser);
				}
			}
		}
	}

	public void StartTimer()
	{
		m_timer = 0;

		m_doOnce = true;

		for( int i = 0; i < m_lasersToActive.Length; i++ )
		{
			m_lasersToActive[i].SetActive(!m_DisableLaser);
		}
	}
}