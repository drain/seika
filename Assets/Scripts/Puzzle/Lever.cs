﻿using UnityEngine;
using System.Collections;

public class Lever : MonoBehaviour {

	private Animator leverAni;


	// Use this for initialization
	void Start () {

		leverAni = GetComponent<Animator> ();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ActivateLever()
	{
		leverAni.SetTrigger ("PlayLever");
	}
}
