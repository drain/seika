﻿using UnityEngine;
using System.Collections;
using Events;

[AddComponentMenu("Trigger/Button")]
public class Button : TriggerEntity 
{
	[SerializeField]
	protected BasicEvent m_onActivate;

	private Animator m_anim;

	protected override void Init ()
	{
		base.Init ();
		m_anim = GetComponent<Animator> ();
	}
		
	protected override void OnActivate ()
	{
		if(m_anim != null)
		{
			m_anim.SetTrigger("Pressed");
		}

		m_onActivate.Invoke();
	}
}