﻿using UnityEngine;
using System.Collections;
using Events;
using Properties;

[AddComponentMenu("Trigger/PressurePlate")]
public class PressurePlate : TriggerEntity 
{
	[SerializeField][BitFlag]
	protected EntityType m_allowedTypes;
	
	[SerializeField]
	protected BoolEvent m_toggleActivation;
	
	protected int m_onPlate;
	protected bool m_activationStatus;
	
	public override bool OnTakeDamage (Damage damage, Vector3 hitPosition)
	{
		// Empty, doesn't use damage
		return true;
	}
	
	protected override void OnActivate ()
	{
		// Empty, doesn't care OnActivate.
		// Uses instead OnCollision Events.
	}	
	
	protected override void OnActivate (bool status)
	{
		m_toggleActivation.Invoke(status);
	}
	
	protected bool CheckFlags(GameObject obj)
	{
		EntityBase entity = obj.GetComponent<EntityBase>();
		
		if (entity)
		{
			return m_allowedTypes.HasFlag(entity.GetEntityType);
		}
		
		return false;
	}
	
	protected virtual void OnCollisionEnter(Collision collision)
	{
		if (CheckFlags(collision.gameObject))
		{
			m_onPlate++;
			
			if (m_onPlate >= 1 && !m_activationStatus)
			{
				
				m_activationStatus = true;
				Activate(true);
			}
		}
	}
	
	protected virtual void OnCollisionExit(Collision collision)
	{
		if (CheckFlags(collision.gameObject))
		{
			m_onPlate--;
			
			if (m_onPlate <= 0 && m_activationStatus)
			{
				m_onPlate = 0;
				m_activationStatus = false;
				Activate(false);
			}
		}
	}
}