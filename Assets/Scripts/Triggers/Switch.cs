﻿using UnityEngine;
using System.Collections;
using Events;

[AddComponentMenu("Trigger/Switch")]
public class Switch : TriggerEntity 
{
	[SerializeField]
	protected bool m_switchStage;

	[SerializeField]
	protected BoolEvent m_onActivate;
	
	protected override void OnActivate ()
	{
		m_switchStage = !m_switchStage;
		m_onActivate.Invoke(m_switchStage);
	}	
}