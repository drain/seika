﻿using UnityEngine;
using System.Collections;

public class Zone : PressurePlate 
{
	protected virtual void OnTriggerEnter(Collider collider)
	{
		if (CheckFlags(collider.gameObject))
		{
			m_onPlate++;
			
			if (m_onPlate >= 1 && !m_activationStatus)
			{
				m_activationStatus = true;
				Activate(true);
			}
		}
	}
	
	protected virtual void OnTriggerExit(Collider collider)
	{
		if (CheckFlags(collider.gameObject))
		{
			m_onPlate--;
			
			if (m_onPlate <= 0 && m_activationStatus)
			{
				m_onPlate = 0;
				m_activationStatus = false;
				Activate(false);
			}
		}
	}

	protected override void OnCollisionExit (Collision collision)
	{
		
	}
	
	protected override void OnCollisionEnter (Collision collision)
	{
		
	}
}