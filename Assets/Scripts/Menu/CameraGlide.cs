﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraGlide : MonoBehaviour 
{
	[SerializeField]
	private GameObject m_camera;

	[SerializeField]
	private Animator m_cameraAnimation;

	[SerializeField]
	private Animator m_panelAnimation;
	
	[SerializeField]
	private Animator m_playPanelAnimation;

	[SerializeField]
	private Animator m_quitAnimation;

	[SerializeField]
	private Animator m_difficultyAnim;

	[SerializeField]
	private GameObject m_playPositionObject;

	private bool m_clickedPlay;

	[SerializeField]
	private Button[] m_mainMenuButtons;

	IEnumerator DelayPanel(float delayTime)
	{
		yield return new WaitForSeconds(delayTime);
		m_playPanelAnimation.SetTrigger("PlayPanelTrigger");
	}

	IEnumerator DelayPanelBack(float delayTime)
	{
		yield return new WaitForSeconds(delayTime);
		m_panelAnimation.SetTrigger("MainPanelTriggerBack");
	}

	IEnumerator DelayDifficulty(float delayTime)
	{
		yield return new WaitForSeconds(delayTime);
		m_difficultyAnim.SetTrigger("Play");
	}

	IEnumerator DelayDifficultyBack(float delayTime)
	{
		yield return new WaitForSeconds(delayTime);
		m_playPanelAnimation.SetTrigger("PlayPanelTrigger");
	}

	IEnumerator QuitGame(float delayTime)
	{
		yield return new WaitForSeconds(delayTime);
		Debug.Log("quit");
		Application.Quit();
	}


	public void ReturnToMainmenu()
	{
		m_cameraAnimation.SetTrigger("TriggerReturn");
		m_playPanelAnimation.SetTrigger("PlayPanelTriggerBack");
		StartCoroutine("DelayPanelBack", 1f);
	}

	public void ClickQuit()
	{
		m_panelAnimation.SetTrigger("MainPanelTrigger");
		m_cameraAnimation.SetTrigger("TriggerQuit");
		StartCoroutine("QuitGame", 1f);

	}

	public void ShowDifficultySelection()
	{
		m_playPanelAnimation.SetTrigger("PlayPanelTriggerBack");
		StartCoroutine("DelayDifficulty", 0.5f);
	}

	public void Back()
	{
		m_difficultyAnim.SetTrigger ("Back");
		StartCoroutine ("DelayDifficultyBack", 1);
	}

	public void ClickPlay()
	{
		m_panelAnimation.SetTrigger("MainPanelTrigger");
		m_cameraAnimation.SetTrigger("Trigger");
		StartCoroutine("DelayPanel", 1f);
	}
}
