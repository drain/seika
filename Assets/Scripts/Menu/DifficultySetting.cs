﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DifficultySetting : MonoBehaviour {




	public int currentDifficulty; // 0 = easy, 1 = normal, 2 = hard

	[SerializeField]
	private Text m_difficultyText;

	[SerializeField]
	private ParticleSystem m_difficultyParticleHard;

	// Use this for initialization
	void Start () {

		currentDifficulty = 0;
		m_difficultyText.text = "Easy";
	
	}
	
	// Update is called once per frame
	void Update () {

		if(currentDifficulty == 2)
		{
			m_difficultyText.text = "Hard";
			for(int i = 0; i < m_difficultyText.text.Length; i++)
			{
				int change = Random.Range(0, 50);
				if(change == 1)
				{
					m_difficultyText.text = "0" + m_difficultyText.text.Substring(Random.Range(0, m_difficultyText.text.Length) + 1);
				}
			}
		}
	}
	

	public void ChangeDifficulty()
	{
		currentDifficulty++;
		if (currentDifficulty > 2)
			currentDifficulty = 0;

		switch (currentDifficulty) 
		{
			case 0:
			{
				m_difficultyParticleHard.Stop();
				m_difficultyText.color = Color.black;
				currentDifficulty = 0;
				m_difficultyText.text = "Easy";
				break;
			}
			case 1:
			{
				currentDifficulty = 1;
				m_difficultyText.text = "Normal";
				break;
			}
			case 2:
			{
				m_difficultyParticleHard.Play();
				currentDifficulty = 2;
				m_difficultyText.text = "Hard";
				m_difficultyText.color = Color.red;
				break;
			}
		}

	}

}
