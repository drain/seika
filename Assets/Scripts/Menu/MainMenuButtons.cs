﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;
using UnityEngine.UI;

public class MainMenuButtons : MonoBehaviour {


	[SerializeField]
	private float delayTime;

	public GameObject pCamera;

	public GameObject[] BloomCubes;
	

	public Image FadeImage;
	public GameObject menuPanel;

	[SerializeField]
	private int transitionTime;


	[SerializeField]
	private int panelTransitionSpeed;

	private float fiAlpha = 0;
	private bool clicked;
	private bool coolTransition;

	private bool doRotate;

	public float minDist;
	public float maxDist;


	private float scale;
	// Use this for initialization
	void Start () {
		pCamera.GetComponent<BloomAndFlares> ().enabled = true;
		pCamera.GetComponent<BloomAndFlares> ().bloomIntensity = 0;

		FadeImage.canvasRenderer.SetAlpha (fiAlpha);

	}
	
	// Update is called once per frame
	void Update () {


		if (clicked) {
			FadeImage.transform.position = new Vector3(76, -9, 42);

			BloomCubes[0].transform.Translate(Vector3.right * 0.1f * Time.deltaTime);
			BloomCubes[1].transform.Translate(-Vector3.right * 0.1f * Time.deltaTime);


		}

		if (doRotate) 
		{
			BloomCubes[0].transform.Rotate(Vector3.right * 5 * Time.deltaTime);
			BloomCubes[1].transform.Rotate(Vector3.left * 5 * Time.deltaTime);
		}


	
	}


	public void ClickPlay()
	{

		clicked = true;
		StartCoroutine("MenuEffect", delayTime);
		StartCoroutine("CubeEffect", 3);
	}


	IEnumerator MenuEffect(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		pCamera.GetComponent<BloomAndFlares> ().bloomIntensity += 0.03f;
		fiAlpha += 0.02f;
		FadeImage.canvasRenderer.SetAlpha (fiAlpha);
		//Application.LoadLevel("level1");
		StartCoroutine("MenuEffect", waitTime);
	}

	IEnumerator addScale(float refTime)
	{
		if (BloomCubes [0].transform.localScale.x > 7) {
			Application.LoadLevel("Level1");
		}
		yield return new WaitForSeconds (refTime);
		scale += 0.005f;
		BloomCubes [0].transform.localScale = new Vector3(BloomCubes [0].transform.localScale.x + scale, 
		                                                  BloomCubes [0].transform.localScale.y + scale, 
		                                                  BloomCubes [0].transform.localScale.z + scale);

		BloomCubes [1].transform.localScale = new Vector3(BloomCubes [1].transform.localScale.x + scale, 
		                                                  BloomCubes [1].transform.localScale.y + scale, 
		                                                  BloomCubes [1].transform.localScale.z + scale);


		StartCoroutine("addScale", refTime);
	}

	IEnumerator CubeEffect(float waitTime)
	{
		yield return new WaitForSeconds (waitTime);
		doRotate = true;
		StartCoroutine("addScale", 0.0005f);


	}
}
