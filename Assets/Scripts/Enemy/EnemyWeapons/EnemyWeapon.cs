using UnityEngine;
using System.Collections;
using Enemy;

public class EnemyWeapon : EnemyWeaponBase
{
	[SerializeField]
	protected GameObject[] m_objects;
	
	[SerializeField]
	protected float m_bonusDuration = 0.1f;
	
	protected Coroutine m_attack;
	
	protected override void Attack (EntityBase entity)
	{
		if (m_attack == null && m_objects.Length != 0)
		{
			m_attack = StartCoroutine(Attack(m_objects, m_attackDuration + m_bonusDuration));
		}
	}
	
	private void SetActive(GameObject[] objs, bool active)
	{
		for (int i = 0; i < objs.Length; i++)
		{
			objs[i].SetActive(active);
		}
	}
	
	private IEnumerator Attack(GameObject[] objs, float duration)
	{
		SetActive(objs, true);
		yield return new WaitForSeconds(duration);
		SetActive(objs, false);
		
		m_attack = null;
	}
}