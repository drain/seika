using UnityEngine;
using System.Collections;
using Enemy;

public class EnemyMelee : EnemyWeaponBase
{
	[SerializeField]
	protected MeleeDamage m_melee;
	
	protected void Awake()
	{
		if (m_melee == null)
		{
			m_melee = GetComponent<MeleeDamage>();
		}
	}
	
	protected override void Attack (EntityBase entity)
	{
		if (m_melee)
		{
			m_melee.Entity = entity;
			m_melee.DoingAttack(true);
			
			Invoke("StopAttack",0.25f);
		}
	}
	
	protected void StopAttack()
	{
		if (m_melee)
		{
			m_melee.DoingAttack(false);
		}
	}
}