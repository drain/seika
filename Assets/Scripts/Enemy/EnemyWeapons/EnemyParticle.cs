using UnityEngine;
using System.Collections;
using Enemy;

public class EnemyParticle : EnemyWeaponBase
{
	[SerializeField]
	protected ParticleSystem m_particleSystem;
	
	protected override void Attack (EntityBase entity)
	{
		m_particleSystem.Play(true);
	}
}