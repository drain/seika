using UnityEngine;
using System.Collections;

namespace Enemy
{
	public abstract class EnemyWeaponBase : MonoBehaviour
	{
		[SerializeField]
		protected float m_attackDelay = 0;
		
		[SerializeField]
		protected float m_animationDuration = 0;
		
		[SerializeField]
		protected float m_attackDuration = 1;
		
		protected bool m_isDoingAttack;
		
		public float GetDuration
		{
			get
			{
				return m_attackDelay + m_animationDuration + m_attackDuration;
			}
		}
		
		protected abstract void Attack(EntityBase entity);
		
		public virtual bool DoAttack(EntityBase entity, Animator anim, string attack = "Attack")
		{
			if (!m_isDoingAttack)
			{
				StartCoroutine(DoAttack(entity, anim, attack, m_attackDelay, m_attackDuration));
				return true;
			}
			
			return false;
		}
		
		protected IEnumerator DoAttack(EntityBase entity, Animator anim, string attack, float delay, float duration)
		{
			m_isDoingAttack = true;
			yield return new WaitForSeconds(delay);
			
			if (anim && attack != "")
			{
				anim.SetTrigger(attack);
				yield return new WaitForSeconds(m_animationDuration);
			}
			
			Attack(entity);
			
			yield return new WaitForSeconds(duration);
			m_isDoingAttack = false;
		}
	}
}