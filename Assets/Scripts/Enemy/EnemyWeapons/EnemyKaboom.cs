using UnityEngine;
using System.Collections;
using Enemy;

public class EnemyKaboom : EnemyWeaponBase
{
	[SerializeField]
	protected ExplosionDamage m_explosion;
	
	protected override void Attack (EntityBase entity)
	{
		GameObject.Instantiate(m_explosion, entity.transform.position, m_explosion.transform.rotation);
		Destroy(gameObject);
	}
}