using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enemy;

public class EnemyProjectile : EnemyWeaponBase
{
	protected struct ProjectileData
	{
		public ProjectileDamage projectile;
		public float lifetime;
		
		public float currentLifetime;
	} 

	[SerializeField]
	protected ProjectileDamage m_projectile;
	
	[SerializeField]
	protected ForceMode m_forceMode = ForceMode.Impulse;
	
	[SerializeField]
	protected float m_projectileLifetime;
	
	[SerializeField]
	protected float m_projectileSpeed;
	
	protected List<ProjectileData> m_projectiles = new List<ProjectileData>();
	
	protected new Transform transform;
	private Coroutine m_lifetimeCoroutine;
	
	protected void Awake()
	{
		transform = GetComponent<Transform>();
	}
	
	
	protected override void Attack (EntityBase entity)
	{

		ProjectileData newProjectile = new ProjectileData(){projectile = ProjectileDamage.Instantiate(m_projectile, transform.position + transform.forward, Quaternion.identity) as ProjectileDamage, lifetime = m_projectileLifetime};
		newProjectile.projectile.Entity = entity;
		newProjectile.projectile.forceSpeed = m_projectileSpeed;
		newProjectile.projectile.forceMode = m_forceMode;
		
		newProjectile.projectile.rigidbody.AddRelativeForce(transform.forward * m_projectileSpeed, m_forceMode);
		
		m_projectiles.Add(newProjectile);
		
		if (m_lifetimeCoroutine == null)
		{
			m_lifetimeCoroutine = StartCoroutine(WaitLifetimes());
		}
	}
	
	protected IEnumerator WaitLifetimes()
	{
		while (m_projectiles.Count != 0)
		{
			for(int index = 0; index < m_projectiles.Count; index++)
			{
				ProjectileData projectileData = m_projectiles[index];
				projectileData.currentLifetime += Time.deltaTime;
				m_projectiles[index] = projectileData;
				
				if (m_projectiles[index].currentLifetime >= m_projectiles[index].lifetime)
				{
					ProjectileData current = m_projectiles[index];
					m_projectiles.RemoveAt(index);
					
					if (current.projectile != null)
					{
						GameObject.Destroy(current.projectile.gameObject);
					}
					
					index--;
					continue;
				}
			}
			
			yield return new WaitForEndOfFrame();
		}
		
		m_lifetimeCoroutine = null;
	}
}