using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enemy;

namespace Boss
{
	[System.Serializable]
	public class BossBehaviourTree : EnemyBehaviourTree<BossBehaviourStage>
	{		
		[SerializeField]
		protected int m_modeID = 0;
		protected int m_lastModeID;
		
		protected Dictionary<int, EnemyBehaviourStageBase[]> m_modes = new Dictionary<int, EnemyBehaviourStageBase[]>();
		
		public override void Init (EnemyAIBase enemy)
		{
			base.Init (enemy);
			
			List<EnemyBehaviourStageBase> stages = new List<EnemyBehaviourStageBase>();
			stages.AddRange(m_fallbackStages.ToArray());
			stages.AddRange(m_stages.ToArray());
			RegisterModes(0,stages.ToArray());
		}
		
		public override void DoUpdate (EnemyAIBase enemy)
		{
			EnemyBehaviourStageBase[] stages;
			
			if (m_modes.TryGetValue(m_modeID, out stages))
			{
				for(int i = 0; i < stages.Length; i++)
				{
					stages[i].DoCondition(enemy, this);
				}
			}
		}
		
		public void RegisterModes(int id, params EnemyBehaviourStageBase[] stages)
		{
			if (stages.Length != 0)
			{
				m_modes.Add(id, stages);
			}
		}
		
		public void SetMode(int id)
		{
			m_modeID = id;
		}
	}
	
	public class BossAIWrapperBase : MonoEnemyBehaviourTree<BossBehaviourTree>
	{
		public BossAI bossAI;
		private Coroutine m_waitMode;
		private int m_modeID;
		
		public virtual bool OnTakeDamage(BossAI boss)
		{
			return true;
		}	
		
		protected void StartMode(int id, float duration)
		{
			if (m_waitMode == null)
			{
				m_waitMode = StartCoroutine(WaitMode(id, duration));
			}	
		}
		
		protected void ForceStopMode(bool skip = false)
		{
			if (m_waitMode != null)
			{
				StopCoroutine(m_waitMode);
				m_waitMode = null;
				
				if (!skip)
				{
					OnModeEnd (m_modeID);
				}
			}
		}
		
		private IEnumerator WaitMode(int id, float duration)
		{
			m_modeID = id;
			CurrentBehaviour.SetMode(id);
			
			yield return new WaitForSeconds(duration);
			
			m_waitMode = null;
			OnModeEnd(id);
		}
		
		protected virtual void OnModeEnd(int id)
		{
		
		}
	}
}