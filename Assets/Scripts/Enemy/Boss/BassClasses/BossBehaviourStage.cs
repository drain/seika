using UnityEngine;
using System.Collections;
using Enemy;

namespace Boss
{
	public abstract class BossBehaviourStage : EnemyBehaviourStage<BossBehaviourTree>
	{
		public delegate void StageDelegate(EnemyAIBase enemy, BossBehaviourTree tree);
		
		public StageDelegate onSuccess;
		public StageDelegate onFail;
		
		public BossBehaviourStage SetOnSuccess(StageDelegate stageDelegate)
		{
			onSuccess = stageDelegate;
			return this;
		}
		
		public BossBehaviourStage SetOnFail(StageDelegate stageDelegate)
		{
			onFail = stageDelegate;
			return this;
		}
		
		protected override void OnSuccess (EnemyAIBase enemy, BossBehaviourTree tree)
		{
			if (onSuccess != null)
			{
				onSuccess(enemy, tree);
			}
		}
		
		protected override void OnFail (EnemyAIBase enemy, BossBehaviourTree tree)
		{
			if (onFail != null)
			{
				onFail(enemy, tree);
			}
		}
	}
}