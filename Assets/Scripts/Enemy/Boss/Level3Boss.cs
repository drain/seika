using UnityEngine;
using System.Collections;
using Enemy;
using Boss;

public sealed class Level3Boss : BossAIWrapperBase
{
	/*
		Stages.
		0. Try hit Player.
		1. Super Awesome Shockwawe!
		2. Get tired.
		3. All again.
	*/
	
	private enum BossStages
	{
		Default,
		Shockwave,
		Tired
	}
	
	[SerializeField]
	private bool m_takeDamage = false;
	
	[SerializeField]
	private EnemyWeapon m_laser;

	[SerializeField]
	private EnemyParticle m_shockwave;
	
	public override void Init (EnemyAIBase enemy)
	{
		BossAttack laser = new BossAttack(m_laser, "Attack", false);
		
		BossAttack shockWawe = new BossAttack(m_shockwave, "Shockwave", false);
		
		CurrentBehaviour.RegisterModes(0, laser);
		CurrentBehaviour.RegisterModes(1, shockWawe);
		
		base.Init (enemy);
		
		StartMode(BossStages.Default.GetValue(), m_laser.GetDuration);
	}	
	
	public override void DoUpdate (EnemyAIBase enemy)
	{
		base.DoUpdate (enemy);
	}
	
	protected override void OnModeEnd (int id)
	{
		BossStages stage = (BossStages)id;
		bossAI.SetFlare(false);
		
		switch(stage)
		{
		case BossStages.Default:
			StartMode(BossStages.Shockwave.GetValue(), 2.6f );
			break;
		
		case BossStages.Shockwave:
			m_takeDamage = true;
			bossAI.SetFlare(true);
			StartMode(BossStages.Tired.GetValue(), 10f);
			break;
		
		case BossStages.Tired:
			StartMode(BossStages.Default.GetValue(), m_laser.GetDuration);
			break;
		}
	}
	
	public override bool OnTakeDamage (BossAI boss)
	{
		if (m_takeDamage)
		{
			m_takeDamage = false;
			
			ForceStopMode(true);
			bossAI.SetFlare(false);
			StartMode(BossStages.Default.GetValue(), m_laser.GetDuration);
			
			return true;
		}
		
		return false;
	}
}