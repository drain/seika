using UnityEngine;
using System.Collections;
using Enemy;
using Boss;

public class BossAI : EnemyAIBase<BossBehaviourTree, BossAIWrapperBase>
{
	[SerializeField]
	protected ParticleSystem m_flare;

	protected override void Init ()
	{
		base.Init ();
		m_currentMonoBehaviourTree.bossAI = this;
	}
	
	protected override void Update ()
	{
		base.Update ();
	}
	
	public override bool OnTakeDamage (Damage damage, Vector3 hitPosition)
	{
		if (damage.Entity.GetEntityType == EntityType.Player)
		{
			if (m_currentMonoBehaviourTree.OnTakeDamage(this))
			{
				base.OnTakeDamage (damage, hitPosition);
			}
		}
	
		return true;
	}
	
	public void SetFlare(bool status)
	{
		if (status)
		{
			m_flare.Play(true);
		}
		else
		{
			m_flare.Stop(true);
		}
	}
	
	public override void OnDie ()
	{
		Destroy(gameObject);
	}
}