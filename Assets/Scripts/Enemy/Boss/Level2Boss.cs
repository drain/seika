using UnityEngine;
using System.Collections;
using Boss;
using Enemy;

public sealed class Level2Boss : BossAIWrapperBase
{
	/*
		Stages
		0. Follow Player
		1. Try Kick Player
		2. Get Tired & Wait.
		3. All Again.
	*/
	
	private enum BossModes
	{
		Default = 0,
		Kick = 1,
		PostKick = 2,
		Tired = 3
	}
	
	[SerializeField]
	private bool m_takeDamage = false;
	
	[SerializeField]
	private bool m_updateTree;
	
	[SerializeField]
	private EnemyWeaponBase m_kick;
	
	[SerializeField]
	private HitDamage m_hit;
	
	private Enemy.Stages.EnemyMove m_moveStage = new Enemy.Stages.EnemyMove();
	
	public override void Init (EnemyAIBase enemy)
	{
	
		if (m_hit != null)
		{
			m_hit.damageCondition = delegate(Damage damage, Vector3 position) {
			
				return enemy.GetEnemyAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Moving") && enemy.GetRigidbody.velocity.sqrMagnitude > 20;
			
			};
		}
	
		m_moveStage.SetSeekStage(null);
		CurrentBehaviour.RegisterStages( m_moveStage );
		CurrentBehaviour.RegisterModes(1, new Boss.BossAttack(m_kick).SetOnSuccess(OnKick).AddCondition(m_moveStage));
		base.Init(enemy);
		
		StartMode(0, 5f);
	}
	
	public override void DoUpdate (EnemyAIBase enemy)
	{
		enemy.GetEnemyAnimator.SetBool("Tired", m_takeDamage);
		
		if (m_updateTree)
		{
			base.DoUpdate (enemy);
		}
		else
		{
			m_moveStage.DoStage(enemy, CurrentBehaviour, true);
		}
	}
	
	protected override void OnModeEnd (int id)
	{
		BossModes mode = (BossModes)id;
		
		m_takeDamage = false;
		bossAI.SetFlare(false);
		
		switch(mode)
		{
		case BossModes.Default:
			StartMode(BossModes.Kick.GetValue(), 0f);
			break;
			
		case BossModes.Kick:
			// Event!
			break;
			
		case BossModes.PostKick:
			m_takeDamage = true;
			bossAI.SetFlare(true);
			StartMode(BossModes.Tired.GetValue(),5f);
			break;
			
		case BossModes.Tired:
			StartMode(BossModes.Default.GetValue(), 10f);

			break;
		}
	}
	
	private void OnKick(EnemyAIBase enemy, BossBehaviourTree tree)
	{
		StartMode(BossModes.PostKick.GetValue(),1.5f);
	}
	
	public override bool OnTakeDamage (BossAI boss)
	{
		boss.GetRigidbody.Sleep();
		boss.GetRigidbody.velocity = Vector3.zero;
		
		if (m_takeDamage)
		{
			ForceStopMode(true);
			StartMode(BossModes.Default.GetValue(),5f);
			bossAI.SetFlare(false);
			m_takeDamage = false;
			return true;
		}
		
		return false;
		
	}
}