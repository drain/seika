using UnityEngine;
using System.Collections;
using Enemy;

namespace Boss
{
	public class BossAttack : BossBehaviourStage
	{
		protected EnemyWeaponBase m_weapon;
		protected string m_attackAnimation;
		protected bool m_look;
		
		public BossAttack(EnemyWeaponBase weapon, string attack = "Attack", bool look = true)
		{
			m_weapon = weapon;
			m_attackAnimation = attack;
			m_look = look;
		}
		
		protected void LookTarget(EnemyAIBase enemy)
		{
			Vector3 rot = Quaternion.LookRotation(-(enemy.GetTransform.position - enemy.GetEnemyTarget.position).normalized, Vector3.up).eulerAngles;
			rot.x = 0;
			
			enemy.GetTransform.rotation = Quaternion.Lerp( enemy.GetTransform.rotation, Quaternion.Euler(rot), 0.5f);
		}
		
		protected override void OnSuccess (EnemyAIBase enemy, BossBehaviourTree tree)
		{
			if (m_look)
			{
				LookTarget(enemy);
			}
			
			if (m_weapon)
			{
				m_weapon.DoAttack(enemy, enemy.GetEnemyAnimator, m_attackAnimation);
			}
			
			base.OnSuccess(enemy, tree);
		}
	
		protected override bool Condition (Enemy.EnemyAIBase enemy, BossBehaviourTree tree)
		{
			return true;
		}
	}
	
}