﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Enemy
{
	public class EnemyBehaviourTree<Stage> : EnemyBehaviourTreeBase where Stage : EnemyBehaviourStageBase
	{
		protected List<Stage> m_stages = new List<Stage>();
		
		public override void DoUpdate (EnemyAIBase enemy)
		{
			for (int i = 0; i < m_stages.Count; i++)
			{
				Debug.Log( m_stages[i].DoCondition(enemy,this) );
			}
			
			base.DoUpdate(enemy);
		}
		
		public void RegisterStages(params Stage[] stages)
		{
			m_stages.AddRange(stages);
		}
	}
}