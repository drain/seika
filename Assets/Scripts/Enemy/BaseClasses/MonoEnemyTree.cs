using UnityEngine;

namespace Enemy
{
	/// <summary>
	/// Mono Behaviour Tree Wrapper.
	/// </summary>
	public class MonoEnemyBehaviourTree<Behaviour>: MonoBehaviour where Behaviour : EnemyBehaviourTreeBase
	{
		[SerializeField]
		protected Behaviour m_behaviour;
		
		public Behaviour CurrentBehaviour
		{
			get
			{
				return m_behaviour;
			}
		}
		
		public virtual void Init(EnemyAIBase enemy)
		{
			m_behaviour.Init(enemy);
		}
		
		public virtual void DoUpdate(EnemyAIBase enemy)
		{
			if (m_behaviour != null)
			{
				m_behaviour.DoUpdate(enemy);
			}
		}
	}
}