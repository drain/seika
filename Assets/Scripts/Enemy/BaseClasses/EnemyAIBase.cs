﻿using UnityEngine;
using System.Collections;

namespace Enemy
{

	/// <summary>
	/// Enemy AI Base.
	/// </summary>
	public class EnemyAIBase : MovingEntity
	{
		[SerializeField]
		protected float m_enemySight;	
		
		[SerializeField]
		protected float m_enemyAttackRange;
		
		[SerializeField]
		protected Transform m_enemyTarget;
		
		protected UnityEngine.AI.NavMeshAgent m_navAgent;
		
		[SerializeField]
		protected EnemyWeaponBase m_enemyWeapon;
		
		[SerializeField]
		protected Animator m_enemyAnimator;
		
		public float GetEnemyAttackRange
		{
			get
			{
				return m_enemyAttackRange;
			}
		}
		
		public float EnemySight
		{
			get
			{
				return m_enemySight;
			}
			set
			{
				m_enemySight = value;
			}
		}
		
		public Transform GetEnemyTarget
		{
			get
			{
				return m_enemyTarget;
			}
		}
		
		public UnityEngine.AI.NavMeshAgent GetEnemeyNavMeshAgent
		{
			get
			{
				return m_navAgent;
			}
		}
		
		public EnemyWeaponBase GetEnemyWeapon
		{
			get
			{
				return m_enemyWeapon;
			}
		}
		
		public Animator GetEnemyAnimator
		{
			get
			{
				return m_enemyAnimator;
			}
		}
		
		public Rigidbody GetRigidbody
		{
			get
			{	
				return base.m_rigidbody;
			}
		}
		
		protected override void Init ()
		{
			GameResources.OnPause +=  delegate(bool isPaused) {
			
				if (m_navAgent)
				{
					if (isPaused)
					{
						m_navAgent.Stop();
					}
					else
					{
						m_navAgent.Resume();
					}
					
				}
			
			};
			
			
			m_navAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
			base.Init ();
		}
		
		protected void Start()
		{
			if (m_enemyTarget == null)
			{
				m_enemyTarget = GameResources.CurrentPlayer.GetTransform;
			}
		}
	}

	/// <summary>
	/// Enemy AI Base.
	/// </summary>
	public class EnemyAIBase<BehaviourTree> : EnemyAIBase where BehaviourTree : EnemyBehaviourTreeBase, new()
	{
		[SerializeField]
		protected BehaviourTree m_currentBehaviourTree;
	
		
		protected override void Init ()
		{
			m_currentBehaviourTree = new BehaviourTree();
			m_currentBehaviourTree.Init(this);
			base.Init ();
		}
		
		protected override void DoUpdate ()
		{
			base.DoUpdate ();
			UpdateBehaviour();
		}
		
		protected virtual void UpdateBehaviour()
		{
			m_currentBehaviourTree.DoUpdate(this);
		}

		public override bool OnTakeDamage (Damage damage, Vector3 hitPosition)
		{
			if(GameResources.CurrentPlayer.UseSharpWeapon)
			{
				Vector3 hitDir = (hitPosition - m_transform.position).normalized;
				hitDir.y = 0;

				if (Vector3.Dot(m_transform.forward, hitDir) < 0)
				{
					damage.GetSetDamage *= 2;
				}
			}

			return base.OnTakeDamage (damage, hitPosition);
		}
	}
	
	/// <summary>
	/// Mono Enemy AI Base.
	/// Wrapper for BehaviourTree using MonoBehaviourTreeWrapper.
	/// </summary>
	public class EnemyAIBase<BehaviourTree,Wrapper> : EnemyAIBase<BehaviourTree> 
														where BehaviourTree : EnemyBehaviourTreeBase, new() 
														where Wrapper : MonoEnemyBehaviourTree<BehaviourTree>, new()
	{
		[SerializeField]
		protected Wrapper m_currentMonoBehaviourTree;
		
		protected override void Init ()
		{
			base.Init ();
			
			if (m_currentMonoBehaviourTree != null)
			{
				m_currentMonoBehaviourTree.Init(this);
				m_currentBehaviourTree = m_currentMonoBehaviourTree.CurrentBehaviour;
			}
			
		}
		
		protected override void UpdateBehaviour ()
		{
			m_currentMonoBehaviourTree.DoUpdate(this);
		}
		
		protected void OnEnable()
		{
			if (m_currentMonoBehaviourTree)
			{
				m_currentMonoBehaviourTree.enabled = true;
			}
		}
		
		protected void OnDisable()
		{
			if (m_currentMonoBehaviourTree)
			{
				m_currentMonoBehaviourTree.enabled = false;
			}
		}
	}
}
