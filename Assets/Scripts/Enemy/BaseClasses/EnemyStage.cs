﻿using UnityEngine;
using System.Collections;

namespace Enemy
{	
	public abstract class EnemyBehaviourStage<BehaviourTree> : EnemyBehaviourStageBase where BehaviourTree : EnemyBehaviourTreeBase, new()
	{
		public EnemyBehaviourStage<BehaviourTree> AddCondition(EnemyBehaviourStageBase condition)
		{
			conditionStage = condition;
			return this;
		}
		
		public EnemyBehaviourStage<BehaviourTree> InititalizeStage(EnemyAIBase enemy)
		{
			Initialize(enemy);
			return this;
		}
		
		/// <summary>
		/// External / Additional Condition.
		/// </summary>
		public EnemyBehaviourStageBase conditionStage;
		
		/// <summary>
		/// Condition rule for Stage.
		/// </summary>
		/// <param name="enemy">Enemy.</param>
		/// <param name="tree">Tree.</param>
		protected abstract bool Condition (EnemyAIBase enemy, BehaviourTree tree);
		
		/// <summary>
		/// Raises the stage event.
		/// Gets called every time.
		/// </summary>
		/// <param name="enemy">Enemy.</param>
		/// <param name="tree">Tree.</param>
		protected virtual void OnStage(EnemyAIBase enemy, BehaviourTree tree) { }
		
		/// <summary>
		/// Raises the success event.
		/// Gets called when Condition returns 'True' value.
		/// </summary>
		/// <param name="enemy">Enemy.</param>
		/// <param name="tree">Tree.</param>
		protected virtual void OnSuccess (EnemyAIBase enemy, BehaviourTree tree) { }
		
		/// <summary>
		/// Raises the fail event.
		/// Gets called when Condition returns 'False' value.
		/// </summary>
		/// <param name="enemy">Enemy.</param>
		/// <param name="tree">Tree.</param>
		protected virtual void OnFail (EnemyAIBase enemy, BehaviourTree tree) { }
		
		protected virtual void Initialize(EnemyAIBase enemy) { }
		
		public void DoStage(EnemyAIBase enemy, BehaviourTree tree, bool stage)
		{
			if (stage)
			{
				OnSuccess(enemy, tree);
			}
			else
			{
				OnFail(enemy, tree);
			}
		}
		
		public override bool DoCondition<BehaviourTreeBase> (EnemyAIBase enemy, BehaviourTreeBase treeBase)
		{
			return DoCondition(enemy, treeBase as BehaviourTree);
		}
		
		public bool DoCondition(EnemyAIBase enemy, BehaviourTree tree)
		{
			if (enemy == null || tree == null)
			{
				return false;
			}
			
			// Every condition needs return 'True' value. (Stacking Conditions)
			// Example Stages:
			// See Target -> Follow Target -> Attack Target
			// OnFail. -> Look Around -> Move -> Move
			// OnSuccess. -> Skip -> Skip -> Attack
			
			bool additionalCondition = (conditionStage != null ? conditionStage.DoCondition(enemy, tree) : true);
			bool condition = false;
			
			if (additionalCondition)
			{
				condition = Condition(enemy, tree);
				
				if (condition)
				{
					OnSuccess(enemy, tree);
				}
				else
				{
					OnFail(enemy, tree);
				}
			}
			
			OnStage(enemy, tree);
			
			return condition;
		}
		
	}
	
	public class EnemyBevaviourStageDelegate<BehaviourTree> : EnemyBehaviourStage<BehaviourTree> where BehaviourTree : EnemyBehaviourTreeBase, new()
	{
		public delegate void StageDelegate(EnemyBevaviourStageDelegate<BehaviourTree> stage, EnemyAIBase enemy, BehaviourTree tree);
		public delegate bool ConditionDelegate(EnemyBevaviourStageDelegate<BehaviourTree> stage, EnemyAIBase enemy, BehaviourTree tree);
		
		public StageDelegate onFail, onSuccess, onStage;
		public ConditionDelegate condition;
		
		public EnemyBevaviourStageDelegate(ConditionDelegate condition, StageDelegate onSuccess = null, StageDelegate onFail = null, StageDelegate onStage = null)
		{
			this.condition = condition;
			this.onSuccess = onSuccess;
			this.onFail = onFail;
			this.onStage = onStage;
		}
		
		protected override bool Condition (EnemyAIBase enemy, BehaviourTree tree)
		{
			if (condition != null)
			{
				condition(this, enemy, tree);
			}
			
			return false;
		}
		
		protected override void OnFail (EnemyAIBase enemy, BehaviourTree tree)
		{
			if (onFail != null)
			{
				onFail(this, enemy, tree);
			}
		}
		
		protected override void OnSuccess (EnemyAIBase enemy, BehaviourTree tree)
		{
			if (onSuccess != null)
			{
				onSuccess(this, enemy, tree);
			}
		}
		
		protected override void OnStage (EnemyAIBase enemy, BehaviourTree tree)
		{
			if (onStage != null)
			{
				onStage(this, enemy, tree);
			}
		}
	}
}