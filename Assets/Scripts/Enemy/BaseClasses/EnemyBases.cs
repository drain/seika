using UnityEngine;
using System.Collections.Generic;
using Enemy;

namespace Enemy
{
	public class EnemyBehaviourTreeBase
	{
		protected List<EnemyBehaviourStage<EnemyBehaviourTreeBase>> m_fallbackStages = new List<EnemyBehaviourStage<EnemyBehaviourTreeBase>>();
		
		public virtual void DoUpdate (EnemyAIBase enemy)
		{
			for (int i = 0; i < m_fallbackStages.Count; i++)
			{
				m_fallbackStages[i].DoCondition(enemy,this);
			}
		}
		
		public virtual void Init(EnemyAIBase enemy)
		{
			
		}
		
		public void RegisterStages(params EnemyBehaviourStage<EnemyBehaviourTreeBase>[] stages)
		{
			m_fallbackStages.AddRange(stages as EnemyBehaviourStage<EnemyBehaviourTreeBase>[]);
		}
	}
	
	public abstract class EnemyBehaviourStageBase 
	{
		public abstract bool DoCondition<BehaviourTreeBase>(EnemyAIBase enemy, BehaviourTreeBase treeBase) where BehaviourTreeBase : EnemyBehaviourTreeBase;
	}
}