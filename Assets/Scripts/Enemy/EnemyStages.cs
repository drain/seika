using UnityEngine;
using System.Collections.Generic;

namespace Enemy
{
	public abstract class EnemyStage : EnemyBehaviourStage<EnemyBehaviourTreeBase>
	{		
		
	}	
	
	namespace Stages
	{
		public class EnemyAttack : EnemyStage
		{
			protected void LookTarget(EnemyAIBase enemy)
			{
				Vector3 rot = Quaternion.LookRotation(-(enemy.GetTransform.position - enemy.GetEnemyTarget.position).normalized, Vector3.up).eulerAngles;
				rot.x = 0;
				
				enemy.GetTransform.rotation = Quaternion.Lerp( enemy.GetTransform.rotation, Quaternion.Euler(rot), 0.5f);
			}
			
			protected override void OnSuccess (EnemyAIBase enemy, EnemyBehaviourTreeBase tree)
			{
				LookTarget(enemy);
				
				if (enemy.GetEnemyWeapon)
				{
					if(enemy.GetEnemyWeapon.DoAttack(enemy, enemy.GetEnemyAnimator))
					{

					}
				}
			}
			
			protected override void OnFail (EnemyAIBase enemy, EnemyBehaviourTreeBase tree)
			{
				LookTarget(enemy);
			}
			
			protected override bool Condition (EnemyAIBase enemy, EnemyBehaviourTreeBase tree)
			{
				return Vector3.Dot(enemy.GetTransform.forward, -(enemy.GetTransform.position - enemy.GetEnemyTarget.position).normalized) > 0.70f;
			}
		}
		
		public class EnemySeek : EnemyStage
		{
			public bool shared;
			
			private EnemyAIBase m_seekerEnemy;
			
			public bool SawPlayer
			{
				get;
				protected set;
			}
			
			protected override void OnSuccess (EnemyAIBase enemy, EnemyBehaviourTreeBase tree)
			{
				SawPlayer = true;
			}
			
			protected override void OnFail (EnemyAIBase enemy, EnemyBehaviourTreeBase tree)
			{
				SawPlayer = false;
			}
			
			protected override bool Condition (EnemyAIBase enemy, EnemyBehaviourTreeBase tree)
			{
				if (enemy == m_seekerEnemy)
				{
					shared = false;
					m_seekerEnemy = null;
				}
				
				Vector3 direction = (enemy.GetTransform.position - enemy.GetEnemyTarget.position);
				float forward = Vector3.Dot(enemy.GetTransform.forward, -direction.normalized);
				
				bool returnValue = (forward > 0.0f || SawPlayer) && direction.sqrMagnitude < enemy.EnemySight;
				
				if (returnValue)
				{
					m_seekerEnemy = enemy;
					shared = true;
				}
				else
				{
					returnValue |= shared;
				}
				
				return returnValue;
			}
		}
		
		public class EnemyMove : EnemyStage
		{
			private static Dictionary<int,EnemySeek> m_shaderSeekerGroups = new Dictionary<int,EnemySeek>();
			
			public static void AddGroup(int index)
			{
				if (!m_shaderSeekerGroups.ContainsKey(index))
				{
					m_shaderSeekerGroups.Add(index, new EnemySeek());
				}
			}
			
			protected EnemySeek m_seekStage;
			
			public EnemyMove(int index = -1)
			{
				if (index != -1)
				{
					AddGroup(index);
					m_seekStage = m_shaderSeekerGroups[index];
				}
				else
				{
					m_seekStage = new EnemySeek();
				}
				
				conditionStage = m_seekStage;
			}
			
			public void SetSeekStage(EnemySeek seek)
			{
				conditionStage = m_seekStage = seek;
				
			}
			
			protected override void OnSuccess (EnemyAIBase enemy, EnemyBehaviourTreeBase tree)
			{
				enemy.GetEnemeyNavMeshAgent.Stop();
				
				if (enemy.GetEnemyAnimator)
				{
					enemy.GetEnemyAnimator.SetBool("Moving", false);
				}
				
			}
			
			protected override void OnFail (EnemyAIBase enemy, EnemyBehaviourTreeBase tree)
			{
				if (m_seekStage == null || m_seekStage.SawPlayer)
				{
					if (enemy.GetEnemyAnimator)
					{
						enemy.GetEnemyAnimator.SetBool("Moving", true);
					
						if(!enemy.GetEnemyAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Moving"))
						{
							enemy.GetEnemeyNavMeshAgent.Stop();
							
							return;
						}
					}
				
					enemy.GetEnemeyNavMeshAgent.Resume();
					enemy.GetEnemeyNavMeshAgent.SetDestination(enemy.GetEnemyTarget.position);
				}
				else
				{
					enemy.GetEnemeyNavMeshAgent.Stop();
				}

			}
			
			protected void Stop(EnemyAIBase enemy, EnemyBehaviourTreeBase tree, UnityEngine.AI.NavMeshAgent agent)
			{
				if ((agent.remainingDistance == 0 || Mathf.Infinity != agent.remainingDistance) && agent.pathStatus == UnityEngine.AI.NavMeshPathStatus.PathComplete)
				{
					enemy.GetEnemeyNavMeshAgent.Stop();
					
					if (enemy.GetEnemyAnimator)
					{
						enemy.GetEnemyAnimator.SetBool("Moving", false);
					}
				}
			}
			
			protected override void OnStage (EnemyAIBase enemy, EnemyBehaviourTreeBase tree)
			{
				if (m_seekStage != null && !m_seekStage.SawPlayer && enemy.GetEnemeyNavMeshAgent.velocity.sqrMagnitude == 0f && (enemy.GetEnemyAnimator != null && enemy.GetEnemyAnimator.GetBool("Moving")))
				{
					Stop(enemy, tree, enemy.GetEnemeyNavMeshAgent);
				}
			}
			
			protected override bool Condition (EnemyAIBase enemy, EnemyBehaviourTreeBase tree)
			{
				return (enemy.GetTransform.position - enemy.GetEnemyTarget.position).sqrMagnitude < enemy.GetEnemyAttackRange;
			}
		}
	}
}