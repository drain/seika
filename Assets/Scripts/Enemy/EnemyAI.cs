﻿using UnityEngine;
using System.Collections;
using Enemy;

public class EnemyAI : EnemyAIBase<EnemyBehaviourTree<EnemyStage>>
{
	[SerializeField]
	private int m_group = -1;
	
	private Vector3 m_hitPosition;
	
	protected override void Init ()
	{
		base.Init ();
		m_currentBehaviourTree.RegisterStages( new Enemy.Stages.EnemyAttack().AddCondition( new Enemy.Stages.EnemyMove(m_group) ) );
	}
	
	public override bool OnTakeDamage (Damage damage, Vector3 hitPosition)
	{
		if (damage.GetDamageType.Equals(Damage.DamageType.Melee))
		{
			m_hitPosition = hitPosition;
			
			if (m_navAgent.isOnNavMesh)
			{
				m_navAgent.SetDestination( m_hitPosition - (GetTransform.position - m_hitPosition) * 2);
			}
		}
		
		if (damage.Entity == null || damage.Entity.GetEntityType.Equals(EntityBase.EntityType.Player))
		{
			Invoke("StopVelocity",0.325f);
			return base.OnTakeDamage(damage, hitPosition);
		}
		
		return true;
	}
	
	protected void StopVelocity()
	{
		m_rigidbody.velocity = Vector3.zero;
		m_rigidbody.angularVelocity = Vector3.zero;
		m_rigidbody.Sleep();
		m_navAgent.velocity = Vector3.zero;
	}
	
	public override void OnDie ()
	{
		base.OnDie();
		LevelManager.RemoveEntity(this);
		Destroy(gameObject);
	}
}
