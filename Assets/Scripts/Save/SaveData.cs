﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

[System.Serializable]
public class SaveData : ISerializable
{
	public string path = "";

	[SerializeField]
	public string currentLevel = "";
	public float time = 0;
	public int difficulty = 0;

	public float xPos = 0;
	public float yPos = 0;
	public float zPos = 0;

	public int currency = 0;

	public List<string> ownedPerks = new List<string>();
	public List<string> ownedUpgrades = new List<string> ();

	public string curWeaponPerk = "";
	public string curPlayerPerk = "";

	#region ISerializable implementation

	public void GetObjectData (SerializationInfo info, StreamingContext context)
	{
		info.AddValue ("currentLevel", currentLevel);
		info.AddValue ("time", time);
		info.AddValue ("difficulty", difficulty);

		info.AddValue ("xPos", xPos);
		info.AddValue ("yPos", yPos);
		info.AddValue ("zPos", zPos);

		info.AddValue ("currency", currency);

		info.AddValue ("ownedPerks", ownedPerks);
		info.AddValue ("ownedUpgrades", ownedUpgrades);

		info.AddValue ("curWeaponPerk", curWeaponPerk);
		info.AddValue ("curPlayerPerk", curPlayerPerk);
	}

	#endregion


	public SaveData(SerializationInfo info, StreamingContext context)
	{
		currentLevel = (string)info.GetValue ("currentLevel", currentLevel.GetType ());
		time = (float)info.GetValue ("time", time.GetType ());
		difficulty = (int)info.GetValue ("difficulty", difficulty.GetType ());

		xPos = (float)info.GetValue ("xPos", xPos.GetType ());
		yPos = (float)info.GetValue ("yPos", yPos.GetType ());
		zPos = (float)info.GetValue ("zPos", zPos.GetType ());

		currency = (int)info.GetValue ("currency", currency.GetType ());

		ownedPerks = (List<string>)info.GetValue ("ownedPerks", ownedPerks.GetType ());
		ownedUpgrades = (List<string>)info.GetValue ("ownedUpgrades", ownedUpgrades.GetType ());

		curWeaponPerk = (string)info.GetValue ("curWeaponPerk", curWeaponPerk.GetType ());
		curPlayerPerk = (string)info.GetValue ("curPlayerPerk", curPlayerPerk.GetType ());
	}
	
	public SaveData()
	{
		
	}
	
	public void Serialize()
	{
		BinaryFormatter bf = new BinaryFormatter();
		MemoryStream ms = new MemoryStream();
		bf.Serialize(ms, this);

		string base64 = System.Convert.ToBase64String(ms.GetBuffer());

		Encryption e = new Encryption ();

		base64 = e.Encrypt (base64);

		File.WriteAllText(path, base64);
		ms.Dispose();
	}
	
	public SaveData DeSerializeSaveData()
	{
		SaveData data = null;
		using (FileStream fileStream = new FileStream (path, FileMode.OpenOrCreate, FileAccess.Read))
		{
			BinaryFormatter bf = new BinaryFormatter();

			StreamReader reader = new StreamReader(path);
			string base64 = reader.ReadToEnd();

			Encryption e = new Encryption ();
			
			base64 = e.Decrypt (base64);

			MemoryStream ms = new MemoryStream( System.Convert.FromBase64String(base64) );
			data = (SaveData) bf.Deserialize(ms);

			ms.Dispose();
		}
		return data;
	}
}