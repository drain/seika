﻿using System.Security.Cryptography;
using System.IO;
using System;
using System.Text;

using UnityEngine;

public class Encryption
{
	private static string key = "48FKLaaSR140dUv3tv4o4AF079x4khwK";//{ 123, 217, 19, 11, 24, 26, 85, 45, 114, 184, 27, 162, 37, 112, 222, 209, 241, 24, 175, 144, 173, 53, 196, 29, 24, 26, 17, 218, 131, 236, 53, 209 };
	private static byte[] vector = {173, 129, 253, 118 , 79, 253, 62, 142, 175, 85, 111,  47, 149, 177, 104, 243};  //{ 146, 64, 191, 111, 23, 3, 113, 119, 231, 121, 221, 112, 79, 32, 114, 156 };
	private ICryptoTransform encryptor, decryptor;
	private UTF8Encoding encoder;

	public Encryption()
	{
		RijndaelManaged rm = new RijndaelManaged();

		encryptor = rm.CreateEncryptor(Convert.FromBase64String (key),vector);
		decryptor = rm.CreateDecryptor(Convert.FromBase64String (key),vector);

		encoder = new UTF8Encoding();
	}
	
	public string Encrypt(string unencrypted)
	{
		return Convert.ToBase64String(Encrypt(encoder.GetBytes(unencrypted)));
	}
	
	public string Decrypt(string encrypted)
	{
		return encoder.GetString(Decrypt(Convert.FromBase64String(encrypted)));
	}
	
	public byte[] Encrypt(byte[] buffer)
	{
		return Transform(buffer, encryptor);
	}
	
	public byte[] Decrypt(byte[] buffer)
	{
		return Transform(buffer, decryptor);
	}
	
	protected byte[] Transform(byte[] buffer, ICryptoTransform transform)
	{
		MemoryStream stream = new MemoryStream();
		using (CryptoStream cs = new CryptoStream(stream, transform, CryptoStreamMode.Write))
		{
			cs.Write(buffer, 0, buffer.Length);
		}
		return stream.ToArray();
	}
}