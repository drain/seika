﻿using UnityEngine;
using System.Collections;

public class SaveSpot : MonoBehaviour 
{
	private UIHandler m_playerUI;

	private void Awake()
	{
		m_playerUI = FindObjectOfType<UIHandler> ();
	}

	private void OnTriggerEnter(Collider collider)
	{
		Character player = collider.GetComponent<Character> ();

		if(player != null)
		{
			m_playerUI.saveButton.gameObject.SetActive(true);
			SaveManager.SaveGame();
		}
	}

	private void OnTriggerExit(Collider collider)
	{
		Character player = collider.GetComponent<Character> ();
		
		if(player != null)
		{
			m_playerUI.saveButton.gameObject.SetActive(false);
		}
	}
}