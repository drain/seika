﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveManager 
{
	private const int SAVE_AMOUNT = 3;

	public static SaveData CurSaveData
	{
		get;
		set;
	}

	private static List<SaveData> m_allSaveDatas = new List<SaveData>();
	public static List<SaveData> AllSaveDatas 
	{
		get
		{
			return m_allSaveDatas;
		}
		set
		{
			m_allSaveDatas = value;
		}
	}

	/// <summary>
	/// Inits the save datas.
	/// Call in the game start once.
	/// </summary>
	public static void InitSaveDatas()
	{
		m_allSaveDatas.Clear ();

		string[] files = Directory.GetFiles(Application.streamingAssetsPath + @"\");

		if(files.Length == 0)
		{
			for( int i = 0; i < SAVE_AMOUNT; i++ )
			{
				FileStream stream = File.Create (Application.streamingAssetsPath + @"\" + "SaveData" + i + ".save");
				stream.Close();
				
				//create new empty save file
				//all variables need to be initialized here!!
				SaveData data = new SaveData();
				data.path = Application.streamingAssetsPath + @"\" + "SaveData" + i + ".save";
				data.currentLevel = "";
				data.difficulty = 1;
				data.time = 0;
				data.xPos = 0;
				data.yPos = 0;
				data.zPos = 0;

				data.currency = 0;

				data.ownedPerks = new List<string>();
				data.ownedUpgrades = new List<string>();

				data.curWeaponPerk = "";
				data.curPlayerPerk = "";
				
				data.Serialize();
				m_allSaveDatas.Add(data);
			}
		}
		else
		{
			for( int i = 0; i < files.Length; i++ )
			{
				if(files[i].Contains(".save") && !files[i].Contains(".meta"))
				{
					SaveData data = new SaveData();
					data.path = files[i];
					data = data.DeSerializeSaveData();
					data.path = files[i];
					m_allSaveDatas.Add(data);
				}
			}
		}
	}

	public static void SaveGame()
	{
		if(CurSaveData != null)
		{
			CurSaveData.time = LevelManager.totalTime;
			CurSaveData.difficulty = LevelManager.DifficultyLevel;
			CurSaveData.currentLevel = Application.loadedLevelName;

			CurSaveData.xPos = GameResources.CurrentPlayer.transform.position.x;
			CurSaveData.yPos = GameResources.CurrentPlayer.transform.position.y;
			CurSaveData.zPos = GameResources.CurrentPlayer.transform.position.z;

			CurSaveData.currency = (int)GameResources.Currency;

			CurSaveData.ownedPerks.Clear();
			foreach( Perk perk in GameResources.CurrentPlayer.ownedPerks )
			{
				CurSaveData.ownedPerks.Add(perk.perkName);
			}

			CurSaveData.ownedUpgrades.Clear();
			foreach( Upgrade upgrade in GameResources.CurrentPlayer.ownedUpgrades )
			{
				CurSaveData.ownedUpgrades.Add(upgrade.upgradeName);
			}

			if( UpgradeSystem.CurWeaponUpgrade != null )
				CurSaveData.curWeaponPerk = UpgradeSystem.CurWeaponUpgrade.upgradeName;
			else
				CurSaveData.curWeaponPerk = "";

			if( UpgradeSystem.CurPlayerUpgrade != null )
				CurSaveData.curPlayerPerk = UpgradeSystem.CurPlayerUpgrade.upgradeName;
			else
				CurSaveData.curPlayerPerk = "";

			CurSaveData.Serialize();
		}
	}
}