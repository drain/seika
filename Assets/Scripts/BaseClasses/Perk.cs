﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public class Perk : IComparable<Perk>, IEquatable<Perk>
{
	public string perkName;
	public int perkId;
	public string desc;
	
	public virtual void DoPerk ()
	{
	}
	
	public int CompareTo (Perk other)
	{
		return 1;
	}

	public bool Equals (Perk other)
	{
		if (other != null)
		{
			return perkName == other.perkName;
		}
		else
		{
			return false;
		}
	}
}