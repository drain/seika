﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public class Upgrade : IEquatable<Upgrade>
{
	public string upgradeName = "";
	public string desc;
	public Color upgradeColor;

	public enum UpradeType
	{
		Weapon,
		Player
	}

	public UpradeType upgradeType;

	public virtual void ApplyUpgrade()
	{

	}

	public virtual void DoUpgrade()
	{

	}

	public virtual void ResetUpgrade()
	{
		
	}
	
	public bool Equals (Upgrade other)
	{
		if(other != null)
		{
			return upgradeName == other.upgradeName;
		}
		else
		{
			return false;
		}
	}
}

[System.Serializable]
public class WeaponUpgrade : Upgrade, IComparable<WeaponUpgrade>
{
	public WeaponUpgrade()
	{
		upgradeType = Upgrade.UpradeType.Weapon;
	}


	#region IComparable implementation

	public int CompareTo (WeaponUpgrade other)
	{
		return 1;
	}

	#endregion

	public override void ApplyUpgrade()
	{
		base.ApplyUpgrade ();
		UpgradeSystem.customizeSystem.UpdateWeaponColor(upgradeColor);
	}

	public override void ResetUpgrade ()
	{
		base.ResetUpgrade ();
		UpgradeSystem.customizeSystem.UpdateWeaponColor(UpgradeSystem.customizeSystem.defaultWeaponColor);
	}
}

[System.Serializable]
public class PlayerUpgrade : Upgrade, IComparable<PlayerUpgrade>
{
	public PlayerUpgrade()
	{
		upgradeType = Upgrade.UpradeType.Player;
	}

	#region IComparable implementation

	public int CompareTo (PlayerUpgrade other)
	{
		return 1;
	}

	#endregion

	public override void ApplyUpgrade()
	{
		base.ApplyUpgrade ();
		UpgradeSystem.customizeSystem.UpdateBodyColor(upgradeColor);
	}

	public override void ResetUpgrade ()
	{
		base.ResetUpgrade ();
		UpgradeSystem.customizeSystem.UpdateBodyColor(UpgradeSystem.customizeSystem.defaultCharacterColor);
	}
}