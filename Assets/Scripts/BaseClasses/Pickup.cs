﻿using UnityEngine;
using System.Collections;

public abstract class Pickup : MonoBehaviour 
{
	protected abstract void OnTriggerEnter(Collider col);
}