﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using Events;

public abstract class TriggerEntity : EntityBase, IDamage
{
	[SerializeField]
	protected float m_delay;
	
	[SerializeField]
	protected bool m_once;
	
	// When Object is waiting for delay.
	private bool m_isActive;
	
	// When Object is used once.
	public bool Activated
	{
		get;
		private set;
	}

	protected abstract void OnActivate();
	protected virtual void OnActivate(bool status) {}

	protected override void Init ()
	{
		m_entityType = EntityType.Trigger;
		base.Init ();
	}

	public virtual bool OnTakeDamage (Damage damage, Vector3 hitPosition)
	{
		if (damage.GetDamageType.Equals(Damage.DamageType.Melee))
		{
			Activate();
		}
		
		return true;
	}
	
	public bool Once()
	{
		return m_once && Activated;
	}
	
	public void Activate(bool status = true)
	{
		if (Once())
		{
			return;
		}
		
		if (!m_isActive)
		{
			StartCoroutine(OnActivate(m_delay, status));
		}
		
		Activated = true;
	}
	
	public void SetActivated(bool status)
	{
		Activated = status;
	}
	
	protected IEnumerator OnActivate(float delay, bool status)
	{
		m_isActive = true;
		yield return new WaitForSeconds(delay);
		m_isActive = false;
		OnActivate(status);
		OnActivate();
	} 
}