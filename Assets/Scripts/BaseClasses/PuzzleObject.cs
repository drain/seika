﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class PuzzleObject : MonoBehaviour, IDamage
{
	[SerializeField]
	protected UnityEvent m_onHit;
	
	[SerializeField]
	protected UnityEvent m_onMeleeHit;
	
	[SerializeField]
	protected UnityEvent m_onRangeHit;
	
	[SerializeField]
	protected UnityEvent m_onInstantHit;
	
	public virtual bool OnTakeDamage (Damage damage, Vector3 hitPosition)
	{
		OnHit(damage);
		return true;
	}
	
	protected virtual void OnHit(Damage damage)
	{
		m_onHit.Invoke();
		
		switch (damage.GetDamageType)
		{
		
		case Damage.DamageType.Melee:
			m_onMeleeHit.Invoke();
			OnMeleeHit (damage);
		break;
		
		case Damage.DamageType.Range:
			m_onRangeHit.Invoke();
			OnRangeHit (damage);
		break;
		
		case Damage.DamageType.Instant:
			m_onInstantHit.Invoke();
			OnInstantHit (damage);
		break;
		
		}
	}
	
	protected virtual void OnMeleeHit(Damage damage)
	{
		
	}
	
	
	protected virtual void OnRangeHit(Damage damage)
	{
	
	}
	
	protected virtual void OnInstantHit(Damage damage)
	{
		
	}
}