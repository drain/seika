﻿using UnityEngine;
using System.Collections;

public class EntityBase : MonoBehaviour 
{
	// Bit flags
	[System.Flags]
	public enum EntityType
	{
		None = (1 << 0),
		Player = (1 << 1),
		Enemy = (1 << 2),
		Entity = (1 << 3),
		Trigger = (1 << 4)
	}
	
	[SerializeField]
	protected EntityType m_entityType;
	
	public EntityType GetEntityType
	{
		get
		{
			return m_entityType;
		}
	}
	
	protected virtual void Init()
	{
	
	}

	protected virtual void DoUpdate()
	{

	}

	protected virtual void DoFixedUpdate()
	{

	}
	
	private void Awake()
	{
		Init();
	}

	protected virtual void Update()
	{
		if (!GameResources.IsPaused)
		{
			DoUpdate();
		}
	}

	protected virtual void FixedUpdate()
	{
		if (!GameResources.IsPaused)
		{
			DoFixedUpdate();
		}
	}
}