using System;
using UnityEngine;

public interface IDamage
{
	/// <summary>
	/// Raises the take damage event.
	/// If return value is
	/// TRUE damage is taken, will stop damage.
	/// FALSE damage is blocked, will continue damage by reflecting it back.
	/// </summary>
	/// <param name="damage">Damage.</param>
	/// <param name="hitPosition">Hit position.</param>
	bool OnTakeDamage(Damage damage, Vector3 hitPosition);
}