﻿using UnityEngine;
using System.Collections;

public abstract class DamageCollider : Damage 
{
	public delegate bool DamageConditionDelegate(Damage damage, Vector3 position);
	
	public DamageConditionDelegate damageCondition;
	
	protected bool CheckDamageCondition(Vector3 position)
	{
		return damageCondition == null || damageCondition(this, position);
	}
}