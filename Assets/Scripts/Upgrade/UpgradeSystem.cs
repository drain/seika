﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class UpgradeSystem 
{
	public static CustomizeSystem customizeSystem;

	public static List<WeaponUpgrade> weaponUpgrades = new List<WeaponUpgrade>();
	public static List<PlayerUpgrade> playerUpgrades = new List<PlayerUpgrade>();

	public static WeaponUpgrade CurWeaponUpgrade
	{
		get;
		set;
	}

	public static PlayerUpgrade CurPlayerUpgrade
	{
		get;
		set;
	}

	public static Upgrade GetUpgradeByName(Upgrade.UpradeType type, string name)
	{
		if(type == Upgrade.UpradeType.Player)
		{
			for( int i = 0; i < playerUpgrades.Count; i++ )
			{
				if( playerUpgrades[i].upgradeName == name)
				{
					return playerUpgrades[i] as PlayerUpgrade;
				}
			}
		}
		else if (type == Upgrade.UpradeType.Weapon)
		{
			for( int i = 0; i < weaponUpgrades.Count; i++ )
			{
				if( weaponUpgrades[i].upgradeName == name)
				{
					return weaponUpgrades[i] as WeaponUpgrade;
				}
			}
		}

		return null;
	}

	public static Upgrade GetUpgradeByName(string name)
	{
		for( int i = 0; i < playerUpgrades.Count; i++ )
		{
			if( playerUpgrades[i].upgradeName == name)
			{
				return playerUpgrades[i];
			}
		}
		for( int i = 0; i < weaponUpgrades.Count; i++ )
		{
			if( weaponUpgrades[i].upgradeName == name)
			{
				return weaponUpgrades[i];
			}
		}
	
		return null;
	}

	public static void InitUpgrades()
	{
		weaponUpgrades = ReflectiveEnumerator.GetEnumerableOfType<WeaponUpgrade> () as List<WeaponUpgrade>;
		playerUpgrades = ReflectiveEnumerator.GetEnumerableOfType<PlayerUpgrade> () as List<PlayerUpgrade>;
	}

	public static void ChangeWeaponUpgrade(string newUpgradeName)
	{
		for( int i = 0; i < weaponUpgrades.Count; i++ )
		{
			if(weaponUpgrades[i].upgradeName == newUpgradeName)
			{
				if(CurWeaponUpgrade != null)
				{
					CurWeaponUpgrade.ResetUpgrade();
				}

				CurWeaponUpgrade = weaponUpgrades[i];
				CurWeaponUpgrade.ApplyUpgrade();

				if(customizeSystem != null)
				{
					customizeSystem.UpdateWeaponColor(CurWeaponUpgrade.upgradeColor);
				}
				return;
			}
		}

		if(CurWeaponUpgrade != null)
		{
			if(customizeSystem != null)
			{
				customizeSystem.UpdateWeaponColor(customizeSystem.defaultWeaponColor);
			}
			CurWeaponUpgrade.ResetUpgrade();
			CurWeaponUpgrade = null;
		}

	}

	public static void ChangePlayerUpgrade(string newUpgradeName)
	{
		for( int i = 0; i < playerUpgrades.Count; i++ )
		{
			if(playerUpgrades[i].upgradeName == newUpgradeName)
			{
				if(CurPlayerUpgrade != null)
				{
					CurPlayerUpgrade.ResetUpgrade();
				}
				
				CurPlayerUpgrade = playerUpgrades[i];
				CurPlayerUpgrade.ApplyUpgrade();
				
				if(customizeSystem != null)
				{
					customizeSystem.UpdateBodyColor(CurPlayerUpgrade.upgradeColor);
				}
				return;
			}
		}
		
		if(CurPlayerUpgrade != null)
		{
			if(customizeSystem != null)
			{
				customizeSystem.UpdateBodyColor(customizeSystem.defaultCharacterColor);
			}
			CurPlayerUpgrade.ResetUpgrade();
			CurPlayerUpgrade = null;
		}

	}
}