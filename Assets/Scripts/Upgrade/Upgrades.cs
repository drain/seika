﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#region WeaponUpgrades

[System.Serializable]
public class BigWeapon : WeaponUpgrade
{
	public BigWeapon()
	{
		upgradeName = "Giant Weapon";
		desc = "Giant Weapon\n\nPlayers weapon becomes bigger";
		upgradeColor = new Color32(0,255,0,255);
	}

	public override void ApplyUpgrade ()
	{
		base.ApplyUpgrade ();
		GameResources.CurrentPlayer.GetComponentInChildren<Weapon>().transform.localScale *= 3;
	}
	public override void ResetUpgrade ()
	{
		base.ResetUpgrade ();

		if(GameResources.CurrentPlayer.GetComponentInChildren<Weapon>().transform.localScale.x > 0.1f)
			GameResources.CurrentPlayer.GetComponentInChildren<Weapon>().transform.localScale /= 3;
	}
}

[System.Serializable]
public class ProjectileWeapon : WeaponUpgrade
{
	public ProjectileWeapon()
	{
		upgradeName = "Shoot Projectiles";
		desc = "Shoot Projectiles\n\nPlayers weapon shoots projectiles when hitting";
		upgradeColor = new Color32(255,0,0,255);
	}
	
	public override void ApplyUpgrade ()
	{
		base.ApplyUpgrade ();
		GameResources.CurrentPlayer.GetComponent<EnemyProjectile>().enabled = true;
	}
	public override void ResetUpgrade ()
	{
		base.ResetUpgrade ();
		GameResources.CurrentPlayer.GetComponent<EnemyProjectile>().enabled = false;
	}
}

[System.Serializable]
public class SharpWeapon : WeaponUpgrade
{
	public SharpWeapon()
	{
		upgradeName = "Sharp Weapon";
		desc = "Sharp Weapon\n\nWeapon deals double damage, when hitting from behind";
		upgradeColor = new Color32(255,255,0,255);
	}
	
	public override void ApplyUpgrade ()
	{
		base.ApplyUpgrade ();
		GameResources.CurrentPlayer.UseSharpWeapon = true;
	}

	public override void ResetUpgrade ()
	{
		base.ResetUpgrade ();
		GameResources.CurrentPlayer.UseSharpWeapon = false;
	}
}

#endregion

#region PlayerUpgrades

[System.Serializable]
public class SpeedPlayer : PlayerUpgrade
{
	public SpeedPlayer()
	{
		upgradeName = "Speed Increase";
		desc = "Speed Increase\n\nPlayer runs a little faster";
		upgradeColor = new Color32(0,0,255,255);
	}

	public override void ApplyUpgrade ()
	{
		base.ApplyUpgrade ();
		GameResources.CurrentPlayer.moveSpeed *= 1.4f;
	}
	public override void ResetUpgrade ()
	{
		base.ResetUpgrade ();
		GameResources.CurrentPlayer.moveSpeed /= 1.4f;
	}
}

[System.Serializable]
public class RespawnPlayer : PlayerUpgrade
{
	public RespawnPlayer()
	{
		upgradeName = "Gift Of Phoenix";
		desc = "Gift Of Phoenix\n\nAfther receiving fatal damage for the first time in a level, ignore it";
		upgradeColor = new Color32(240,120,12,255);
	}
	
	public override void ApplyUpgrade ()
	{
		base.ApplyUpgrade ();
	}
	public override void ResetUpgrade ()
	{
		base.ResetUpgrade ();
	}
}

[System.Serializable]
public class Stealth : PlayerUpgrade
{
	public Stealth()
	{
		upgradeName = "Shroud Of Shadows";
		desc = "Shroud Of Shadows\n\nEnemies have smaller detection range";
		upgradeColor = new Color32(129,129,129,255);
	}
	
	public override void ApplyUpgrade ()
	{
		base.ApplyUpgrade ();
		GameResources.CurrentPlayer.UseStealth = true;
	}

	public override void ResetUpgrade ()
	{
		base.ResetUpgrade ();
		GameResources.CurrentPlayer.UseStealth = false;
	}

	public override void DoUpgrade ()
	{
		base.DoUpgrade ();

		List<EnemyAI> enemyAI = new List<EnemyAI>();

		foreach(MovingEntity me in LevelManager.Instance.MovingEntities)
		{
			EnemyAI e = me.GetComponent<EnemyAI>();

			if(e != null)
			{
				enemyAI.Add(e);
			}
		}

		if(enemyAI.Count > 0)
		{
			foreach(EnemyAI e in enemyAI)
			{
				e.EnemySight /= 5;
			}
		}
	}
}

[System.Serializable]
public class DashDamage : PlayerUpgrade
{
	public DashDamage()
	{
		upgradeName = "Power Dash";
		desc = "Power Dash\n\nDash deals damage to enemies";
		upgradeColor = new Color32(255,0,0,255);
	}
	
	public override void ApplyUpgrade ()
	{
		base.ApplyUpgrade ();
		GameResources.CurrentPlayer.DashDamage = true;

	}

	public override void ResetUpgrade ()
	{
		base.ResetUpgrade ();
		GameResources.CurrentPlayer.DashDamage = false;
	}
}
#endregion