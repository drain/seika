using UnityEngine;
// Editor Only.
#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// Dialog asset.
/// For Dialog Manager.
/// </summary>
public class DialogAsset : ScriptableObject 
{
	[System.Serializable]
	public class Dialog
	{
		public Character person;

		public Sprite picture;

		[Multiline]
		public string message;

		public string[] answers;

		public DialogAsset firstOption;
		public DialogAsset secondOption;

		public bool openStore;
	}

	public enum DialogType
	{
		Message,
		Choice
	}

	public enum Character
	{
		Seika,
		Merchant,
		AI
	}
	
	public Dialog[] dialogs;



}
