﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class DialogManager : MonoBehaviour {

	public static DialogManager CurrentManager 
	{
		get;
		private set;
	}

	public DialogAsset currentDialog;
	public DialogHelper helper;
	public Text nameText;
	public Text dialogText;
	public Text[] answerText;
	public int dialogCounter = 0;
	public Canvas dialogCanvas;

	public bool needAnswer = false;
	public GameObject shopCanvas;
	


	public Image talkPic;

	private bool m_shopOpen;

	// Use this for initialization
	void Awake () {
		CurrentManager = this;
	}

	void Start()
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyUp (KeyCode.Space) && dialogCanvas.enabled == true && needAnswer == false)
		{
			dialogCounter++;

			if (currentDialog.dialogs.Length <= dialogCounter) {
				dialogCounter = 0;
				dialogCanvas.enabled = false;
				DialogHelper.isTalking = false;
				GameResources.IsPaused = false;
			} 
			else 
			{
				UpdateDialog();
			}
		}
	}

	public void FirstOption ()
	{
		DialogHelper.isTalking = false;
		currentDialog = currentDialog.dialogs [dialogCounter].firstOption;
		dialogCounter = 0;
		UpdateDialog ();
	}
	public void SecondOption ()
	{
		DialogHelper.isTalking = false;
		currentDialog = currentDialog.dialogs [dialogCounter].secondOption;
		dialogCounter = 0;
		UpdateDialog ();
	}

	public void UpdateDialog()
	{
		talkPic.sprite = currentDialog.dialogs [dialogCounter].picture;
		nameText.text = currentDialog.dialogs[dialogCounter].person.ToString ();
		dialogText.text = currentDialog.dialogs [dialogCounter].message;

		if (currentDialog.dialogs [dialogCounter].openStore == true && shopCanvas != null) 
		{
			shopCanvas.SetActive(true);
			m_shopOpen = true;
		}

		if(currentDialog.dialogs [dialogCounter].openStore == false && shopCanvas != null && m_shopOpen)
		{
			shopCanvas.SetActive(false);
			m_shopOpen = false;
			SaveManager.SaveGame();
		}

		if (currentDialog.dialogs [dialogCounter].answers.Length > 0) 
		{
			needAnswer = true;
			for (int i = 0; i < answerText.Length; i++) {
				answerText[i].enabled = true;
				answerText [i].text = currentDialog.dialogs [dialogCounter].answers [i];
			}
		} 

		if(currentDialog.dialogs [dialogCounter].answers.Length == 0)
		{
			needAnswer = false;
			answerText[0].enabled = false;
			answerText[1].enabled = false;
		}
	}
}
