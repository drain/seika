﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Dialog tester.
/// Lol this is for only testing.
/// </summary>
public class DialogHelper : MonoBehaviour {

	public DialogAsset currentDialog;
	private DialogManager manager;

	public static bool isTalking = false;


	// Use this for initialization
	void Start () 
	{
		manager = FindObjectOfType<DialogManager> ();
	}

	public void SetDialog()
	{
		if (isTalking == false)
		{
			isTalking = true;
			GameResources.IsPaused = true;
			DialogManager.CurrentManager.currentDialog = currentDialog;
			manager.UpdateDialog ();
			manager.dialogCanvas.enabled = true;
		}
	}
}
