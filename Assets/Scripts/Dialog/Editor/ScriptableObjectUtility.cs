﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

public static class ScriptableObjectUtility  
{
	/// <summary>
	//	This makes it easy to create, name and place unique new ScriptableObject asset files.
	/// </summary>
	public static void CreateAsset<T> () where T : ScriptableObject
	{
		// Create new ScriptableObject.
		T asset = ScriptableObject.CreateInstance<T> ();

		// Get current path.
		string path = AssetDatabase.GetAssetPath (Selection.activeObject);
		if (path == "") 
		{
			path = "Assets";
		} 
		else if (Path.GetExtension (path) != "") 
		{
			path = path.Replace (Path.GetFileName (AssetDatabase.GetAssetPath (Selection.activeObject)), "");
		}
		
		string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath (path + "/New " + typeof(T).ToString() + ".asset");

		// Create final asset.
		AssetDatabase.CreateAsset (asset, assetPathAndName);

		// Save asset.
		AssetDatabase.SaveAssets ();
		AssetDatabase.Refresh();
		EditorUtility.FocusProjectWindow ();

		// Set selected object to asset.
		Selection.activeObject = asset;
	}

	[MenuItem("Assets/Create/Dialog")]
	public static void CreateAsset ()
	{
		// Create new DialogAsset.
		ScriptableObjectUtility.CreateAsset<DialogAsset> ();
	}
}
