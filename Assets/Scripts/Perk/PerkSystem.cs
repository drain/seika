﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class PerkSystem
{
	public static Perk GetPerkById(int id)
	{
		for( int i = 0; i < AllPerks.Count; i++ )
		{
			if( AllPerks[i].perkId == id )
			{
				return AllPerks[i];
			}
		}

		return null;
	}

	public static Perk GetPerkByName(string name)
	{
		InitList ();
		for( int i = 0; i < AllPerks.Count; i++ )
		{
			if( AllPerks[i].perkName == name)
			{
				return AllPerks[i];
			}
		}
		
		return null;
	}

	public static List<Perk> AllPerks 
	{
		get;
		private set;
	}

	public static void InitList()
	{
		AllPerks = ReflectiveEnumerator.GetEnumerableOfType<Perk> () as List<Perk>;
	}
}