﻿/// <summary>
/// Add perks here.
/// </summary>
 
using UnityEngine;
using System.Collections;

[System.Serializable]
public class TeleportTime : Perk 
{
	public TeleportTime()
	{
		perkName = "Rapid Teleportation";
		perkId = 0;
		desc = "Rapid Teleportation\n\nPlayer teleports twice as fast, but teleport range is decreased.";
	}

	public override void DoPerk ()
	{
		GameResources.CurrentPlayer.teleportDistance /= 1.75f;
		GameResources.CurrentPlayer.teleportTime /= 2;
	}
}

[System.Serializable]
public class TeleportLenght : Perk 
{
	public TeleportLenght()
	{
		perkName = "Long Distance";
		perkId = 1;
		desc = "Long Distance\n\nPlayer teleports longer distance, but teleport cooldown is increased.";
	}

	public override void DoPerk ()
	{
		GameResources.CurrentPlayer.teleportDistance *= 2;
		GameResources.CurrentPlayer.teleportTime *= 2;
	}
}

[System.Serializable]
public class GoFast : Perk 
{
	public GoFast()
	{
		perkName = "Gotta go fast";
		perkId = 2;
		desc = "Gotta go fast\n\nPlayer runs twice as fast, but cannot teleport.";
	}
	
	public override void DoPerk ()
	{
		LevelManager.AllowPlayerTeleport = false;
		GameResources.CurrentPlayer.moveSpeed *= 1.5f;
	}
}

[System.Serializable]
public class Knocback : Perk 
{
	public Knocback()
	{
		perkName = "Improved Knockback";
		perkId = 3;
		desc = "Improved Knockback\n\nPlayer and Enemies knocbacks are double effective.";
	}
	
	public override void DoPerk ()
	{
		for( int i = 0; i < LevelManager.Instance.HealthEntities.Count; i++ )
		{
			LevelManager.Instance.HealthEntities[i].Force *= 2f;
		}
	}
}

[System.Serializable]
public class DamagePerk : Perk 
{
	public DamagePerk()
	{
		perkName = "Improved Damage";
		perkId = 4;
		desc = "Improved Damage\n\nPlayer and Enemies deal double damage.";
	}
	
	public override void DoPerk ()
	{
		Damage.damageMultiplier *= 2;
	}
}