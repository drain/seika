﻿using UnityEngine;
using System.Collections;

public class DropPickup : MonoBehaviour 
{
	public enum DropType
	{
		Health,
		Currency,
		Other,
		Empty
	}

	private Transform m_transform;
	private int m_amountToDrop;

	[SerializeField]
	private GameObject[] m_pickUpPrefabs;

	[SerializeField]
	private int minAmountOfDrops;
	
	[SerializeField]
	private int maxAmountOfDrops;
	
	[System.Serializable]
	public class DropItem
	{
		public DropPickup.DropType dropType;
		public int amount;
		public bool randomizeAmount;
		public int min,max;
		public float dropChance;
	}
	
	public DropItem[] itemsToDrop;
	
	public bool addForce;
	private float maximumPropability;

	[SerializeField]
	private float m_forceAmount; 
	
	private void Start()
	{
		m_transform = transform;
		m_amountToDrop = Random.Range( minAmountOfDrops,maxAmountOfDrops );
		InitItemChances ();
	}
	/// <summary>
	/// Initializes drop chances
	/// </summary>
	private void InitItemChances()
	{
		for( int i = 0;i<itemsToDrop.Length;i++ )
		{
			if(i != 0)
			{
				itemsToDrop[i].dropChance += itemsToDrop[i - 1].dropChance;
			}
		}
		
		maximumPropability = itemsToDrop[itemsToDrop.Length - 1].dropChance;
		
		for( int i = 0;i<itemsToDrop.Length;i++ )
		{
			int temp = System.Convert.ToInt32(((maximumPropability / 100) * itemsToDrop[i].dropChance));
			itemsToDrop[i].dropChance = (float)temp;
		}
		
		maximumPropability = itemsToDrop[itemsToDrop.Length - 1].dropChance;
	}
	/// <summary>
	/// Drops the items.
	/// </summary>
	public void DropItems()
	{
		for( int i = 0;i<m_amountToDrop;i++ )
		{
			int rnd = Random.Range(0,(int)maximumPropability + 1);
			DropItem droppedItem = ChooseItem(rnd);
			if( droppedItem.dropType != DropType.Empty)
			{
				SpawnItem(droppedItem);
			}
		}	
	}
	/// <summary>
	/// Spawns item.
	/// </summary>
	/// <param name="droppedItem">Dropped item.</param>
	private void SpawnItem(DropItem droppedItem)
	{
		GameObject temp = GameObject.Instantiate( m_pickUpPrefabs[GetIndexOfItemPrefab(droppedItem)],m_transform.position + new Vector3(0,m_transform.localScale.y + .75f,0),Quaternion.identity ) as GameObject;

		Currency currency = temp.GetComponent<Currency>();
		HealthPickup heal = temp.GetComponent<HealthPickup> ();

		//Set currency
		if(currency != null && currency.currencyAmount == 0)
		{
			if(droppedItem.randomizeAmount)
			{
				currency.currencyAmount = Random.Range(droppedItem.min,droppedItem.max + 1);
			}
			else
			{
				currency.currencyAmount = droppedItem.amount;
			}
		}

		//Set heal
		if(heal != null && heal.healAmount == 0)
		{
			if(droppedItem.randomizeAmount)
			{
				heal.healAmount = Random.Range(droppedItem.min,droppedItem.max + 1);
			}
			else
			{
				heal.healAmount = droppedItem.amount;
			}
		}

		if(addForce)
		{
			temp.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-1f,1f),1,Random.Range(-1f,1f)).normalized * m_forceAmount,ForceMode.Impulse);	
		}
	}

	/// <summary>
	/// Gets the index of prefab to drop
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="droppedItem">Dropped item.</param>
	private int GetIndexOfItemPrefab(DropItem droppedItem)
	{
		for( int i = 0; i < m_pickUpPrefabs.Length; i++ )
		{
			if(m_pickUpPrefabs[i].GetComponent<HealthPickup>() && droppedItem.dropType == DropType.Health)
			{
				return i;
			}

			if(m_pickUpPrefabs[i].GetComponent<Currency>() && droppedItem.dropType == DropType.Currency)
			{
				return i;
			}
		}
		return 0;
	}
	
	/// <summary>
	/// Choose item to drop by its drop chance
	/// </summary>
	/// <returns>The item.</returns>
	/// <param name="rnd">Random.</param>
	private DropItem ChooseItem(int rnd)
	{
		int maxVal = (int)itemsToDrop [itemsToDrop.Length - 1].dropChance;
		
		if(rnd == maxVal)
			rnd -= 1;
		
		int index = 0;
		while (itemsToDrop[index].dropChance <= rnd)
		{
			index++;
		}
		return itemsToDrop [index];
	}

}