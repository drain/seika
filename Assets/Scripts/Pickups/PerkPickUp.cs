﻿using UnityEngine;
using System.Collections;

public class PerkPickUp : Pickup 
{
	public string perkName = "";

	protected override void OnTriggerEnter (Collider col)
	{
		Character player = col.GetComponent<Character> ();

		if(player != null && !player.ownedPerks.Contains(PerkSystem.GetPerkByName(perkName)))
		{
			player.ownedPerks.Add(PerkSystem.GetPerkByName(perkName));
		}
	}
}