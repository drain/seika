﻿using UnityEngine;
using System.Collections;

public class HealthPickup : Pickup 
{
	public int healAmount;

	protected override void OnTriggerEnter(Collider col)
	{
		Character player = col.GetComponent<Character> ();
		if(player != null && player.CurrentHealth < player.MaxHealth)
		{
			player.DoHeal(healAmount);
			Destroy(gameObject);
		}
	}
}