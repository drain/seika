﻿using UnityEngine;
using System.Collections;

public class Currency : Pickup 
{
	public int currencyAmount;
	
	protected override void OnTriggerEnter(Collider col)
	{
		Character player = col.GetComponent<Character> ();
		if(player != null)
		{
			GameResources.Currency += currencyAmount;
			UIHandler.UpdateCurrencyText(currencyAmount);
			Destroy(gameObject);
		}
	}
}