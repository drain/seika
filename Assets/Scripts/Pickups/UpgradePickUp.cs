﻿using UnityEngine;
using System.Collections;

public class UpgradePickUp : Pickup 
{
	public string upgradeName = "";
	public Upgrade.UpradeType upgradeType;

	protected override void OnTriggerEnter (Collider col)
	{
		Character player = col.GetComponent<Character> ();
		
		if(player != null && !player.ownedUpgrades.Contains(UpgradeSystem.GetUpgradeByName(upgradeType,upgradeName)))
		{
			player.ownedUpgrades.Add(UpgradeSystem.GetUpgradeByName(upgradeType,upgradeName));
		}

		Destroy (gameObject);
	}
}