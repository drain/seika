using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Weapon : HitDamage 
{
	private Material m_weaponMat;

	private Character m_owner;

	private ParticleSystem m_weaponParticles;
	
	private List<GameObject> m_hittedObjects = new List<GameObject>();

	private Animator m_ownerAnim;

	private bool m_doOnce;

	protected override void Awake ()
	{
		base.Awake ();
		
		m_weaponMat = GetComponent<Renderer> ().material;
		m_owner = transform.root.GetComponent<Character> ();
		m_weaponParticles = GetComponentInChildren<ParticleSystem> ();
		m_ownerAnim = m_owner.GetComponent<Animator> ();
	}
	
	public void ChangeWeaponColor(Color weaponColor)
	{
		m_weaponMat.SetColor ("_EmissionColor", weaponColor * 5f);
		m_weaponParticles.startColor = weaponColor;
	}

	private void FixedUpdate()
	{
		if(!m_owner.IsDoingAttack)
		{
			m_doOnce = false;
			m_hittedObjects.Clear();
		}

		if(m_ownerAnim.GetCurrentAnimatorStateInfo(0).IsName("Attack2") && !m_doOnce)
		{
			m_doOnce = true;

			GameObject[] colliders = m_hittedObjects.ToArray();
			m_hittedObjects.Clear();
			
			foreach(GameObject col in colliders)
			{
				if (col != null)
				{
					OnTriggerEnter(col.GetComponent<Collider>());
				}
			}
		}
	}

	protected override void OnTriggerEnter(Collider collider)
	{
		if(m_owner.IsDoingAttack && collider.gameObject.tag != "MainCamera")
		{
			IDamage damage = GetDamageInterface(collider.gameObject);

			if(!m_hittedObjects.Contains(collider.gameObject) && OnDealDamage(damage, m_owner.transform.position).Equals(DamageReturnType.True))
			{
				m_hittedObjects.Add (collider.gameObject);
			}
		}
	}
}