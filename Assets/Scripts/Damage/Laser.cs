﻿using UnityEngine;
using System.Collections;

public class Laser : Damage 
{	
	[SerializeField]
	protected bool m_active;
	
	[SerializeField]
	protected bool m_alwaysBounce;
	
	[SerializeField][Range(1,100)]
	protected int m_laserBounces = 0;

	[SerializeField][Range(0,100)]
	protected float m_laserDistance = 10;
			
	[SerializeField]
	protected LayerMask m_laserMask;

	[SerializeField]
	protected LineRenderer m_lineRenderer;

	[SerializeField]
	protected float m_lineRendererDuration = 5f;
	
	protected Ray m_ray;
	
	public void ToggleActive()
	{
		SetActive(!m_active);
	}
	
	public void SetActive(bool active)
	{
		m_lineRenderer.enabled = m_active = active;
	}
	
	public void ShootLaser()
	{		
		m_ray.origin = m_transform.position;
		m_ray.direction = m_transform.forward;

		if (m_lineRenderer)
		{
			m_lineRenderer.SetVertexCount(1 + m_laserBounces);
			m_lineRenderer.SetPosition(0, m_ray.origin);
		}

		RaycastHit? oldHit = null;
	
		for(int i = 0; i < m_laserBounces; i++)
		{
			RaycastHit hit;
			
			if (oldHit != null)
			{
				//m_ray.direction = ((m_ray.origin - oldHit.Value.point).normalized - oldHit.Value.normal) * -1;
				m_ray.direction = Vector3.Reflect(m_ray.direction, oldHit.Value.normal);
				m_ray.origin = oldHit.Value.point;
			}
			
			if (Physics.Raycast(m_ray, out hit, m_laserDistance, m_laserMask))
			{
				IDamage damage = GetDamageInterface(hit.collider.gameObject);
				
				m_lineRenderer.SetPosition(i + 1, hit.point);
				
				if (damage != null)
				{
					OnDealDamage(damage, hit.point);
					
					if (!m_alwaysBounce)
					{
						// End Bounces.
						m_lineRenderer.SetVertexCount(i + 2);
						break;
					}
				}
	
				oldHit = hit;
			}
			else
			{
				m_lineRenderer.SetVertexCount(i + 2);
				m_lineRenderer.SetPosition(i + 1, m_ray.GetPoint(m_laserDistance));
				
				break;
			}
		}
		
		if (m_lineRenderer)
		{
			m_lineRenderer.enabled = true;
			
			if (!m_active)
			{
				StartCoroutine(DeActiveLaser(m_lineRendererDuration));
			}
		}
	}

	protected override void Awake ()
	{
		base.Awake ();
		
		m_type = DamageType.Instant;
		m_lineRenderer.enabled = m_active;
	}
	
	protected virtual void FixedUpdate()
	{
		if (m_active)
		{
			ShootLaser();
		}
	}
	
	protected IEnumerator DeActiveLaser(float delay)
	{
		yield return new WaitForSeconds(delay);
		m_lineRenderer.enabled = false;
	}
}