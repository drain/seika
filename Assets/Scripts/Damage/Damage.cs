using UnityEngine;
using System.Collections;

public abstract class Damage : MonoBehaviour
{
	public static float damageMultiplier = 1;

	public enum DamageType
	{
		Melee,
		Range,
		Instant
	}
	
	protected enum DamageReturnType
	{
		Null,
		True,
		False
	}

	[SerializeField]
	protected EntityBase m_entity;

	[SerializeField]
	protected int m_damage;
	
	[SerializeField]
	protected DamageType m_type;
	
	protected Transform m_transform;
	
	public int GetSetDamage
	{
		get
		{
			return m_damage * (int)damageMultiplier;
		}
		set
		{
			m_damage = value;
		}
	}
	
	public DamageType GetDamageType
	{
		get
		{
			return m_type;
		}
	}
	
	public EntityBase Entity
	{
		get
		{
			return m_entity;
		}
		
		set
		{
			if (value != null)
			{
				m_entity = value;
				
				if (CreatorEntity == null)
				{
					CreatorEntity = m_entity;
				}
			}
		}
	}
	
	public EntityBase CreatorEntity
	{
		get;
		private set;
	}
	
	public IDamage GetDamageInterface(GameObject go)
	{
		return (IDamage)go.GetComponent(typeof(IDamage));
	}
	
	protected virtual DamageReturnType OnDealDamage(IDamage damage, Vector3 hitPosition)
	{
		if (damage != null)
		{
			return damage.OnTakeDamage(this, hitPosition) ? DamageReturnType.True : DamageReturnType.False;
		}
		
		return DamageReturnType.Null;
	}
	
	protected virtual void Awake()
	{
		m_transform = GetComponent<Transform>();
		
		if (m_entity == null)
		{
			m_entity = GetComponent<EntityBase>();
		}
	}
}