using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleDamage : DamageCollider
{
	protected override void Awake ()
	{
		m_type = DamageType.Range;
		base.Awake ();
	}

	protected virtual void OnParticleCollision(GameObject other)
	{
		IDamage damage = GetDamageInterface(other);
		Vector3 position = other.transform.position - (other.transform.position - m_transform.position).normalized;
		
		if (CheckDamageCondition(position))
		{
			OnDealDamage(damage, position);
		}
	}
}