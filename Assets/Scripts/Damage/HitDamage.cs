﻿using UnityEngine;
using System.Collections;

public class HitDamage : DamageCollider 
{
	protected override void Awake ()
	{
		m_type = DamageType.Melee;
		base.Awake ();
	}
	
	protected virtual void OnTriggerEnter(Collider collider)
	{
		IDamage damage = GetDamageInterface(collider.gameObject);
		Vector3 position = m_transform.position;
		
		if (CheckDamageCondition(position))
		{
			OnDealDamage(damage, position);
		}
	}
}