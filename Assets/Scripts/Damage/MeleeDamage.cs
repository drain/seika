using UnityEngine;
using System.Collections;

public class MeleeDamage : HitDamage
{
	[SerializeField]
	protected bool m_isDoingAttack;
	
	public void DoingAttack(bool status)
	{
		m_isDoingAttack = status;
	}
	
	protected override void OnTriggerEnter (Collider collider)
	{

	}
	
	protected virtual void OnTriggerStay(Collider collider)
	{
		if (m_isDoingAttack)
		{
			base.OnTriggerEnter (collider);
		}
	}
}