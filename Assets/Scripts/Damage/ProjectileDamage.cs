﻿using UnityEngine;
using System.Collections;

public class ProjectileDamage : DamageCollider 
{
	public new Rigidbody rigidbody;
	public float forceSpeed = 1;
	public ForceMode forceMode;
	
	[SerializeField]
	protected bool m_destoryOnHit = true;
	protected bool m_reflect;
	
	protected override void Awake ()
	{
		rigidbody = GetComponent<Rigidbody>();
		m_type = DamageType.Range;
		base.Awake ();
	}
	
	
	protected virtual void OnCollisionEnter(Collision collision)
	{
		IDamage damage = GetDamageInterface(collision.gameObject);
		Vector3 position = collision.contacts[0].point;
		
		if (CheckDamageCondition(position))
		{
			OnDealDamage(damage, position);
		}
		
		if (m_destoryOnHit && !m_reflect)
		{
			Destroy(gameObject);
		}
		
		m_reflect = false;
	}
	
	protected override DamageReturnType OnDealDamage (IDamage damage, Vector3 hitPosition)
	{
		DamageReturnType returnType = base.OnDealDamage (damage, hitPosition);
		
		if (returnType.Equals(DamageReturnType.False))
		{
			m_reflect = true;
			
			Vector3 velocity = -rigidbody.velocity.normalized;
			
			if (CreatorEntity != null)
			{
				Vector3 creatorPosition = CreatorEntity.transform.position;
				velocity = (creatorPosition - rigidbody.position).normalized;
				velocity.y /= 2;
			}
			
			rigidbody.AddForce( velocity * forceSpeed, forceMode);
		}
		
		return returnType;
	}
}