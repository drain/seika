using UnityEngine;
using System.Collections;

public class ExplosionDamage : Damage
{
	[SerializeField]
	protected float m_radius = 2f;
	
	[SerializeField]
	protected LayerMask m_mask;
	
	[SerializeField]
	protected float m_lifetime = 5f;
	
	protected IEnumerator Start()
	{
		OnExplosion();
		
		yield return new WaitForSeconds(m_lifetime);
		Destroy(gameObject);
	}
	
	protected virtual void OnExplosion()
	{
		Collider[] objects = Physics.OverlapSphere(m_transform.position, m_radius, m_mask);
		
		for(int i = 0; i < objects.Length; i++)
		{
			OnDealDamage(GetDamageInterface(objects[i].gameObject), m_transform.position);
		}
	}
}