using System;
using UnityEngine;

namespace Properties
{
	public static class PropertyHelper
	{
		public static bool HasFlag<T>(this T enumValue, T enumLookingValue) where T : System.IComparable
		{
			int value = (int) (object) enumValue;
			int looking = (int) (object) enumLookingValue;
			
			if( (value & looking) == looking)
			{
				return true;
			}
			
			return false;
		}
	}
	
	public class BitFlag : PropertyAttribute
	{
		
	}
}