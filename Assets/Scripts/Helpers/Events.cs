﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace Events
{
	[System.Serializable]
	public class BoolEvent : UnityEvent<bool>
	{
		
	}
	
	[System.Serializable]
	public class BasicEvent : UnityEvent
	{
	
	}
}