using System;
using System.Collections;
using UnityEngine;


public static class CommonUtilities
{
	public static int GetValue<Enum>(this Enum e) where Enum: System.IComparable
	{
		return (int)(object)e;
	}
}