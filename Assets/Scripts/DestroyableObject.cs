﻿using UnityEngine;
using System.Collections;

public class DestroyableObject : HealthEntity 
{
	public override void OnDie ()
	{
		base.OnDie ();
		LevelManager.RemoveEntity(this);
		Destroy(gameObject);
	}
}