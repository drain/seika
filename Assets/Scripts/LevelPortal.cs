﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelPortal : MonoBehaviour 
{
	private UIHandler m_playerUI;

	[SerializeField]
	private string m_levelToLoad;
	
	private void Awake()
	{
		m_playerUI = FindObjectOfType<UIHandler> ();
	}
	
	private void OnTriggerEnter(Collider collider)
	{
		Character player = collider.GetComponent<Character> ();
		
		if(player != null && m_playerUI != null)
		{
			LevelManager.levelToLoad = m_levelToLoad;

			m_playerUI.changeLevelButton.gameObject.SetActive(true);
			m_playerUI.changeLevelButton.GetComponentInChildren<Text>().text = "To " + m_levelToLoad;
		}
	}
	
	private void OnTriggerExit(Collider collider)
	{
		Character player = collider.GetComponent<Character> ();
		//LevelManager.levelToLoad = "";
		
		if(player != null)
		{
			m_playerUI.changeLevelButton.gameObject.SetActive(false);
		}
	}
}