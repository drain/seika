// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/StencilDiffuseDoubleSided"
{
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_BackColor ("Back Color", Color) = (0,0,0,1)
	}
	SubShader {

		Pass
		{
			Stencil
			{
				Ref 1
				Comp Equal
				Pass Keep
			}
			
	         ZWrite Off // don't write to depth buffer 
	            // in order not to occlude other objects
 			 Cull Off
	         Blend SrcAlpha OneMinusSrcAlpha // use alpha blending
	 
	         CGPROGRAM 
	 
	         #pragma vertex vert 
	         #pragma fragment frag
	         #include "UnityCG.cginc"
	         
	         uniform float4 _LightColor0; 
	         
       		 uniform float4 _Color;
	         uniform sampler2D _MainTex;
	         
	         uniform float3 _EmissionColor;
	         uniform sampler2D _EmissionMap;
	       
	 
		    struct vertexInput {
	            float4 vertex : POSITION;
	            float4 texcoord : TEXCOORD0;
	            float3 normal : NORMAL;
	         };
	         struct vertexOutput {
	            float4 pos : SV_POSITION;
	            float4 tex : TEXCOORD0;
	            float4 col : COLOR;
	         };
	 
	         vertexOutput vert(vertexInput input) 
	         {
	            vertexOutput output;
	 
	            output.tex = input.texcoord;
	               // Unity provides default longitude-latitude-like 
	               // texture coordinates at all vertices of a 
	               // sphere mesh as the input parameter 
	               // "input.texcoord" with semantic "TEXCOORD0".
	            output.pos = UnityObjectToClipPos(input.vertex);
	            
	            
                float4x4 modelMatrix = unity_ObjectToWorld;
       		 	float4x4 modelMatrixInverse = unity_WorldToObject; 
               // multiplication with unity_Scale.w is unnecessary 
               // because we normalize transformed vectors
	            
	            float3 normalDirection = normalize(
	               mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);
	            float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
	 
	            float3 diffuseReflection = _LightColor0.rgb * _Color.rgb
	               * max(0.0, dot(normalDirection, lightDirection));
	            
             output.col = float4(diffuseReflection, 1.0);
	            
	            return output;
	         }
	         
	         float4 frag(vertexOutput input) : COLOR
	         {
	         	float4 emission = tex2D(_EmissionMap, input.tex.xy);
	            return float4(tex2D(_MainTex, input.tex.xy).rgb * _Color.rgb * input.col + emission.rgb * _EmissionColor, 0.25 * _Color.a);	
	               // look up the color of the texture image specified by 
	               // the uniform "_MainTex" at the position specified by 
	               // "input.tex.x" and "input.tex.y" and return it
	 
	         }
	 
	         ENDCG  
      	}


		Tags { "RenderType"="Opaque" "Queue"="Transparent-1" }
		LOD 200
		Cull back
		Stencil
		{
			Ref 1
			Comp NotEqual
			Pass Keep
		}
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
		
		Tags { "RenderType"="Opaque" "Queue"="Transparent-1" }
		LOD 200
		Cull front
		Stencil
		{
			Ref 1
			Comp NotEqual
			Pass Keep
		}
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _BackColor;
		
		void vert (inout appdata_full v) {
          v.normal *= -1;
      }

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _BackColor;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG

	} 
	FallBack "Diffuse"
}
