﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/StencilBlockMask"
{
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }

		Pass 
		{
			//Colormask 0
			ZWrite Off
			Cull Front
			Blend SrcAlpha OneMinusSrcAlpha 
			
			Stencil
			{
				Ref 1
				Comp Equal
				Pass IncrSat
			}
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			uniform half4 _Color;
			
			struct input
			{
				float4 vertex : POSITION;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
			};
			
				v2f vert (input v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					return o;
				}
				
				half4 frag (v2f i) : COLOR
				{
					return _Color;
				}
	
			ENDCG
		}
	} 
}
