﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/StencilOutline" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_UnlitOutlineThickness("_UnlitOutlineThickness", Float) = -1
		_LitOutlineThickness("LitOutlineThickness", Float) = 1
		
		_EmissionColor("Emission Color", Color) = (0,0,0)
		_EmissionMap("Emission", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		Pass
		{
			Stencil
			{
				Ref 1
				Comp Equal
				Pass Keep
			}
			
	         ZWrite Off // don't write to depth buffer 
	            // in order not to occlude other objects
 			 Cull Off
	         Blend SrcAlpha OneMinusSrcAlpha // use alpha blending
	 
	         CGPROGRAM 
	 
	         #pragma vertex vert 
	         #pragma fragment frag
	         #include "UnityCG.cginc"
	         
	         uniform float4 _LightColor0; 
	         
       		 uniform float4 _Color;
	         uniform sampler2D _MainTex;
	 
		    struct vertexInput {
	            float4 vertex : POSITION;
	            float4 texcoord : TEXCOORD0;
	            float3 normal : NORMAL;
	         };
	         struct vertexOutput {
	            float4 pos : SV_POSITION;
	            float4 tex : TEXCOORD0;
	            float3 normalDir : TEXCOORD1;
	            float3 normal : TEXCOORD2;
	            float4 col : COLOR;
	         };
	 
	 		uniform float4 _OutlineColor;
 		 	uniform half _UnlitOutlineThickness;
 		 	uniform half _LitOutlineThickness;
 		 	
	         uniform float3 _EmissionColor;
	         uniform sampler2D _EmissionMap;
	 
	         vertexOutput vert(vertexInput input) 
	         {
	            vertexOutput output;
	 
	            output.tex = input.texcoord;
	               // Unity provides default longitude-latitude-like 
	               // texture coordinates at all vertices of a 
	               // sphere mesh as the input parameter 
	               // "input.texcoord" with semantic "TEXCOORD0".
	            output.pos = UnityObjectToClipPos(input.vertex);
	            
	            
                float4x4 modelMatrix = unity_ObjectToWorld;
       		 	float4x4 modelMatrixInverse = unity_WorldToObject; 
               // multiplication with unity_Scale.w is unnecessary 
               // because we normalize transformed vectors
	            
	            float3 normalDirection = normalize(mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);
	            
	            float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
	            //normalDirection = input.normal; 

	            float3 diffuseReflection = _LightColor0.rgb * _Color.rgb
	               * max(0.0, dot(normalDirection, lightDirection));
	            
             	output.col = float4(diffuseReflection, 1.0) + 0.2;
             	output.normalDir = normalDirection;
             	output.normal = input.normal;
	            
	            return output;
	         }
	         
	         float4 frag(vertexOutput input) : COLOR
	         {
            	float3 viewDirection = normalize(_WorldSpaceCameraPos - input.tex.xyz);
	            float3 normalDirection = normalize(input.normalDir);
	            float3 normal = normalize(input.normal);
	            
                float3 lightDirection;
	            float attenuation;
	 
	            if (0.0 == _WorldSpaceLightPos0.w) // directional light?
	            {
	               attenuation = 1.0; // no attenuation
	               lightDirection = normalize(_WorldSpaceLightPos0.xyz);
	            } 
	            else // point or spot light
	            {
	               float3 vertexToLightSource = _WorldSpaceLightPos0.xyz - input.tex.xyz;
	               float distance = length(vertexToLightSource);
	               attenuation = 1.0 / distance; // linear attenuation 
	               lightDirection = normalize(vertexToLightSource);
	            }
	        
	         	float4 emission = tex2D(_EmissionMap, input.tex.xy);
	            float4 output = float4(tex2D(_MainTex, input.tex.xy).rgb * input.col * _Color.rgb + emission.rgb * _EmissionColor, 0.25 * _Color.a);	
	            
	            if (dot(viewDirection, normalDirection) < lerp(_UnlitOutlineThickness, _LitOutlineThickness, max(0.0, dot(normalDirection, lightDirection))))
	            {
	              output.rgb = _EmissionColor.rgb; 
	            }

	            return output;
	         }
	 
	         ENDCG  
      	}
      	
		Cull back
		Stencil
		{
			Ref 1
			Comp NotEqual
			Pass Keep
		}
					
		Pass
		{
	         CGPROGRAM 
	 
	         #pragma vertex vert 
	         #pragma fragment frag
	         #include "UnityCG.cginc"
	         
	         uniform float4 _LightColor0; 
	         
       		 uniform float4 _Color;
	         uniform sampler2D _MainTex;
	 
		    struct vertexInput {
	            float4 vertex : POSITION;
	            float4 texcoord : TEXCOORD0;
	            float3 normal : NORMAL;
	         };
	         struct vertexOutput {
	            float4 pos : SV_POSITION;
	            float4 tex : TEXCOORD0;
	            float3 normalDir : TEXCOORD1;
	            float3 normal : TEXCOORD2;
	            float4 col : COLOR;
	         };
	 
	 		uniform float4 _OutlineColor;
 		 	uniform half _UnlitOutlineThickness;
 		 	uniform half _LitOutlineThickness;
 		 	
	         uniform float3 _EmissionColor;
	         uniform sampler2D _EmissionMap;
	 
	         vertexOutput vert(vertexInput input) 
	         {
	            vertexOutput output;
	 
	            output.tex = input.texcoord;
	               // Unity provides default longitude-latitude-like 
	               // texture coordinates at all vertices of a 
	               // sphere mesh as the input parameter 
	               // "input.texcoord" with semantic "TEXCOORD0".
	            output.pos = UnityObjectToClipPos(input.vertex);
	            
	            
                float4x4 modelMatrix = unity_ObjectToWorld;
       		 	float4x4 modelMatrixInverse = unity_WorldToObject; 
               // multiplication with unity_Scale.w is unnecessary 
               // because we normalize transformed vectors
	            
	            float3 normalDirection = normalize(mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);
	            
	            float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
	            //normalDirection = input.normal; 

	            float3 diffuseReflection = _LightColor0.rgb * _Color.rgb
	               * max(0.0, dot(normalDirection, lightDirection));
	            
             	output.col = float4(diffuseReflection, 1.0) + 0.2;
             	output.normalDir = normalDirection;
             	output.normal = input.normal;
             	
//	            half _UnlitOutlineThickness = 0;
//	            half _LitOutlineThickness = _OutlineSize;
//	            
//            	float3 viewDirection = normalize(_WorldSpaceCameraPos - output.tex.xyz);
//	            
//	            if (dot(viewDirection, normalDirection) < lerp(_UnlitOutlineThickness, _LitOutlineThickness, max(0.0, dot(normalDirection, lightDirection))))
//	            {
//	               output.col.rgb = _OutlineColor.rgb; 
//	            }
	 
	            
	            return output;
	         }
	         
	         float4 frag(vertexOutput input) : COLOR
	         {
            	float3 viewDirection = normalize(_WorldSpaceCameraPos - input.tex.xyz);
	            float3 normalDirection = normalize(input.normalDir);
	            float3 normal = normalize(input.normal);
	            
                float3 lightDirection;
	            float attenuation;
	 
	            if (0.0 == _WorldSpaceLightPos0.w) // directional light?
	            {
	               attenuation = 1.0; // no attenuation
	               lightDirection = normalize(_WorldSpaceLightPos0.xyz);
	            } 
	            else // point or spot light
	            {
	               float3 vertexToLightSource = _WorldSpaceLightPos0.xyz - input.tex.xyz;
	               float distance = length(vertexToLightSource);
	               attenuation = 1.0 / distance; // linear attenuation 
	               lightDirection = normalize(vertexToLightSource);
	            }
	        
	         	float4 emission = tex2D(_EmissionMap, input.tex.xy);
	            float4 output = float4(tex2D(_MainTex, input.tex.xy).rgb * input.col * _Color.rgb + emission.rgb * _EmissionColor,_Color.a);	
	            
	            if (dot(viewDirection, normalDirection) < lerp(_UnlitOutlineThickness, _LitOutlineThickness, max(0.0, dot(normalDirection, lightDirection))))
	            {
	              output.rgb = _EmissionColor.rgb; 
	            }

	            return output;
	         }
	 
	         ENDCG  
      	}

	} 
	FallBack "Standard"
}
