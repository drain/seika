﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/StencilMask" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" "Queue"="Geometry" }

		Pass 
		{
			//Colormask 0
			ZWrite off
			//ZTest NotEqual
			Cull off
			Blend SrcAlpha OneMinusSrcAlpha 
			
			Stencil
			{
				Ref 1
				Comp Always
				Pass Replace
			}
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			uniform half4 _Color;
			
			struct input
			{
				float4 vertex : POSITION;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
			};
			
				v2f vert (input v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					return o;
				}
				
				half4 frag (v2f i) : COLOR
				{
					return _Color;
				}
	
			ENDCG
		}
		
		Pass 
		{
			Colormask 0
			ZWrite Off
			ZTest Less
			Cull Front
			Blend SrcAlpha OneMinusSrcAlpha 
			
			Stencil
			{
				Ref 1
				Comp Equal
				Pass IncrSat
			}
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			uniform half4 _Color;
			
			struct input
			{
				float4 vertex : POSITION;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
			};
			
				v2f vert (input v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					return o;
				}
				
				half4 frag (v2f i) : COLOR
				{
					return _Color;
				}
	
			ENDCG
		}
	} 
}
