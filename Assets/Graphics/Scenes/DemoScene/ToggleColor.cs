﻿using UnityEngine;
using System.Collections;

public class ToggleColor : MonoBehaviour 
{
	[SerializeField]
	private Color m_normal = Color.white;
	
	[SerializeField]
	private Color m_hit = Color.red;
	
	[SerializeField]
	private bool m_isHitted;
	
	private Renderer m_renderer;
	
	public void OnHit()
	{
		m_isHitted = true;
	}
	
	private void Awake()
	{
		m_renderer = GetComponent<Renderer>();
	}
	
	private void FixedUpdate()
	{
		Color color = m_normal;
		
		if (m_isHitted)
		{
			color = m_hit;
		}
		
		m_renderer.material.SetColor("_Color", color);
	
		m_isHitted = false;
	}
}