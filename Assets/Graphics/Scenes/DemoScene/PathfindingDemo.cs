﻿using UnityEngine;
using System.Collections;

public class PathfindingDemo : MonoBehaviour 
{
	[SerializeField]
	private Transform m_target;
	
	private UnityEngine.AI.NavMeshAgent m_agent;
	
	private void Awake()
	{
		m_agent = GetComponent<UnityEngine.AI.NavMeshAgent>();	
	}
	
	private void FixedUpdate()
	{
		m_agent.SetDestination(m_target.position); 
	}
	
	
}