﻿using UnityEngine;
using System.Collections;

public class Stencil : MonoBehaviour 
{
	[SerializeField]
	private Transform m_target;

	[SerializeField]
	private float m_maxDistance = 10;
	
	[SerializeField] 
	private float m_defaultScale = 10;
	
	[SerializeField]
	private Vector3 m_scale = Vector3.one;
	
	[SerializeField]
	private bool m_both;
	
	[SerializeField]
	private LayerMask m_mask;
	
	private new Transform transform;
	
	private bool m_hit;
	
	private void Awake()
	{
		transform = GetComponent<Transform>();
	}
	
	private void Start()
	{
		if (m_target == null)
		{
			m_target = GameResources.CurrentPlayer.GetTransform;
		}
	}
	
	private void FixedUpdate () 
	{
		m_hit = false;
		
		//transform.localPosition = Vector3.zero;
		//transform.Translate(m_offset);
		transform.rotation = Quaternion.Euler(Vector3.zero);
		
		Vector3 scale = m_scale * m_defaultScale;
		Vector3 position = m_target.position;
		
		float x1;
		float x2;
		
		bool xHit;
		if (AxisCondition(CheckLine(Vector3.right, out x1), CheckLine(-Vector3.right, out x2), out xHit))
		{
			scale.x = (x1 + x2) * m_scale.x;
		}
		
		float z1;
		float z2;
		
		bool zHit;
		if (AxisCondition(CheckLine(Vector3.forward, out z1), CheckLine(-Vector3.forward, out z2), out zHit))
		{
			scale.z = (z1 + z2) * m_scale.z;
		}
		
		position.x -= x2 / 2 - x1 / 2;
		position.z -= z2 / 2 - z1 / 2;
		
		if (!xHit && !zHit)
		{
			scale  = Vector3.zero;
		}
	
		transform.localScale = scale;
		transform.position = position;

	}
	
	private bool AxisCondition(bool axis1, bool axis2, out bool hit)
	{
		bool axis = m_both ? axis1 & axis2 : axis1 | axis2;
		
		if (m_both)
		{
			hit = axis;
		}
		else
		{
			hit = m_hit;
		}
		
		return axis;
	}
	
	/*
	private void OnDrawGizmos()
	{
		Vector3 position = m_target.position;
		Vector3 scale = m_scale * m_maxDistance;
		
		float x1;
		float x2;
		
		if (CheckLine(Vector3.right, out x1) | CheckLine(-Vector3.right, out x2))
		{
			scale.x = x1 + x2;
			//position.x = (position.x - x2;
			
			Gizmos.DrawRay(position, Vector3.right * x1);
			Gizmos.DrawRay(position, -Vector3.right * x2);
			

			//position.x = 
		}
		
		float z1;
		float z2;
		
		if (CheckLine(Vector3.forward, out z1) | CheckLine(-Vector3.forward, out z2))
		{
			scale.z = z1 + z2;
			//position.z -= scale.z / 2;
			
			Gizmos.DrawRay(position, Vector3.forward * z1);
			Gizmos.DrawRay(position, -Vector3.forward * z2);
		}
		
		
		position.x -= x2 / 2 - x1 / 2;
		position.z -= z2 / 2 - z1 / 2;
		
		Gizmos.DrawCube(position, scale);
	}
	*/
	
	private bool CheckLine(Vector3 direction, out float distance)
	{
		RaycastHit hit;
		
		if (Physics.Raycast(m_target.position, direction, out hit, m_maxDistance, m_mask))
		{
			m_hit = true;
			
			distance = Mathf.Abs(hit.distance);
			return true;
		}
		
		distance = m_defaultScale;
		return false;
	}
}
