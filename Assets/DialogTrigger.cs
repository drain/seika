﻿using UnityEngine;
using System.Collections;

public class DialogTrigger : MonoBehaviour {

	public DialogHelper helper;

	// Use this for initialization
	void Start () {

		helper = this.gameObject.GetComponent<DialogHelper> ();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			helper.SetDialog();
		}
	}
}
